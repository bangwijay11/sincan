import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import React, { useContext } from 'react';
import StackNavigator from './app/store/router/StackNavigator';
import { store } from './app/store/redux';
import { AuthContext } from './app/store/router/AuthenticationProvider';
import auth from '@react-native-firebase/auth';

const App = () => {
  // const [user, setUser] = useContext(AuthContext);
  // const [initilizing, setInitilizing] = useState(true);

  // const onAuthStateChanged = (user) => {
  //   setUser(user);
  //   if (initilizing) setInitilizing(false);
  // };
  // useEffect(() => {
  //   const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  //   return subscriber;
  // }, []);
  // if (initializing) return null;

  return (
    <Provider store={store}>
      <NavigationContainer>
        <StackNavigator />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
