import { Help, Lesson, NewsLetter } from '../../property'
import { Text, View } from 'react-native'

import React from 'react'
import { StyleSheet } from 'react-native'
import { SwiperContent } from '..'

const index = () => {
    return (
        <View>
            <View>
                <View style={styles.Conten}>
                    <NewsLetter width={500} height={300}/>
                </View>
               <SwiperContent judul="Dapatkan Bertia Terbaru" desc="Dapatkan Berita terbaru tentang bencana alam" />
            </View>
            <View>
              <View style={styles.Conten}>
                <Help width={500} height={300}/>
              </View>
             <SwiperContent judul="Selamatkan Diri" desc="Lakukan Panggilan darurat jika terjadi bencana dan gunakan maps untuk mencari tempat yang lebih aman"/>
            </View>
            <View>
              <View style={styles.Conten}>
                <Lesson width={400} height={300}/>
              </View>
             <SwiperContent judul="Mulai Belajar" desc="Mulailah belajar dengan pembelajaran yang telah kami sediakan"/>
            </View>
        </View>
    )
}

export default index
const styles = StyleSheet.create ({

      Conten:{
        width: '100%',
         height: '80%',
           alignItems: 'center', 
           justifyContent: 'center'
      }
    
})
