import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

import { BackButton } from '..';
import { BackIcon } from '../../property'
import { Image } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';

const index = (props) => {
    // const navigation = useNavigation();

    return (
    <View style={styles.boxClass}>
        <View style={styles.backContainer}>
            <BackButton/>
        </View>

          <View style={styles.boxContainer}>
            <View style={styles.imgBox}>
              <Image
                source={props.img}
                style={styles.ilus}
              />
            </View>
            <View style={styles.textBox}>
              <Text style={styles.textBoxHeader}>{props.title}</Text>
            </View>
          </View>
      </View>
    )
}

export default index
const styles = StyleSheet.create({
    boxClass: {
      width: '100%',
      height: 200,
      backgroundColor: '#2A89B4',
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
    },
    backContainer:{marginTop:0},
    iconBack: {
        marginTop: 20, 
        marginLeft: 20
    },
    boxContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    ilus: {
        width: 107,
        height: 117,
    },
    textBox: {
        width: 150,
        height: 48,
    },
    textBoxHeader: {
      fontWeight: '700', 
      fontSize: 20, 
      color: 'white',
      marginLeft: 8
    },

})
