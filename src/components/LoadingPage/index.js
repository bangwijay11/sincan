import {Image, StatusBar, StyleSheet, View} from 'react-native';

import React from 'react';

const index = () => {
    useEffect(() => {
        setTimeout(() => {
          navigation.replace('Home');
        }, 2000);
      });
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <ActivityIndicator size="small" color="#0000ff" />
      
    </View>
  );
};
export default index;

const styles = StyleSheet.create ({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:  1,
  }
})
