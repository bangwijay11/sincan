import {Button, StyleSheet, Text, View} from 'react-native';
import {
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import { BackIcon } from '../../../property';
import { CardSubChapter } from '../../../components';
import React from 'react';
import {ScrollView} from 'react-native';

// import FontAwesome from 'react-native-vector-icons/FontAwesome';
const SettingsScreen = props => {
  return (
    <ImageBackground
      source={require('../../../property/images/bgsubcourse.png')}
      style={styles.container}>
      <StatusBar backgroundColor="white" />
      <View>
      <TouchableOpacity onPress={() => props.navigation.goBack()}>
              <BackIcon style={{marginTop: 20, marginLeft: 20}} width={24} height={24} />
            </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginRight: 31,
        }}>
        <View style={{marginLeft: 60, marginTop: 26}}>
          <Image
            source={require('../../../property/images/earthquake.png')}
            style={{width: 100, height: 100}}
          />
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 24,
              fontWeight: '700',
              color: 'white',
              fontFamily: 'Quicksand',
              marginRight: 20,
            }}>
            Gempa Bumi
          </Text>
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        <CardSubChapter
          move={() => props.navigation.navigate('Course')}
          title="Pengertian gempa"
          
          imgBgBencana={require('../../../property/images/Intersect.png')}
          imgBencana={require('../../../property/images/earthquake2.png')}
        />
        <CardSubChapter
          title="Penyebab gempa"
          imgBgBencana={require('../../../property/images/Intersect.png')}
          imgBencana={require('../../../property/images/earthquake2.png')}
        />
        <CardSubChapter
          title="Jenis-jenis gempa"
          imgBgBencana={require('../../../property/images/Intersect.png')}
          imgBencana={require('../../../property/images/earthquake2.png')}
        />
        <CardSubChapter
          title="Tanda-tanda gempa"
          imgBgBencana={require('../../../property/images/Intersect.png')}
          imgBencana={require('../../../property/images/earthquake2.png')}
        />
      </ScrollView>
    </ImageBackground>
  );
};

export default SettingsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  back: {
    marginTop: 24,
    marginLeft: 20,
  },

  title: {
    color: 'black',
    fontFamily: 'Quicksand',
    fontSize: 14,
    fontWeight: '700',
    fontStyle: 'normal',
  },
});
