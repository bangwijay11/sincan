import { Text, View } from 'react-native'

import { NewsLetter } from '../../property'
import React from 'react'
import { TEXT_ONBOARDING } from '../FontStyle/FontStyle'

const index = (props) => {
    return (
        <View>
            <View style={{ alignItems: 'center', marginTop: -20}}>
                    <Text style={TEXT_ONBOARDING}>{props.judul}</Text>
                    <View style={{width: 300, marginTop: 15}}>
                    <Text style={{fontSize: 12, color: '#434242', textAlign: 'center' }}>{props.desc}</Text>
                </View>
            </View>
        </View>
    )
}

export default index
