import { ActivityIndicator, Text, View } from 'react-native'

import React from 'react'

const index = () => {
    return (
        <View>
            <ActivityIndicator color="red" size="large" />
        </View>
    )
}

export default index
