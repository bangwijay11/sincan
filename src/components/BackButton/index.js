import * as React from 'react';

import { Button, StyleSheet } from 'react-native';

import { BackIcon } from '../../property';
import { TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

function index() {
  const navigation = useNavigation();

  return (
    <TouchableOpacity onPress={() => navigation.goBack()}>
        <BackIcon style={styles.iconBack} width={24} height={24} />
    </TouchableOpacity>
  );
}

export default index
const styles = StyleSheet.create({
    iconBack: {
        marginTop: 30, 
        marginLeft: 20
    }
})
