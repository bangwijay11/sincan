import React, {useState} from 'react'
import { SUBTITLE_COURSE, TITLE_COURSE } from '../FontStyle/FontStyle';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'

import { BottonLesson } from '..';
import LinearGradient from 'react-native-linear-gradient';

const index = (props) => {
  return (
    <LinearGradient colors={['#74EBD5', '#ACB6E5']} style={{ width: '100%', borderRadius: 30, alignItems: 'center', marginBottom: 0 }}>
      <ScrollView showsVerticalScrollIndicator={false} style={{ width: '80%', marginTop: 23, marginBottom: 15 }}>
        <Text style={TITLE_COURSE}>{props.bab}</Text>
        <View style={{ marginTop: 8 }}>
          <View>
            <Text style={SUBTITLE_COURSE}>{props.desc} </Text>
          </View>
        </View>
        {/* <View style={{ marginTop: 20 }}>
          <BottonLesson title={props.titleBtn} move={props.onPress} />
        </View> */}
      </ScrollView>
    </LinearGradient>
  )
}

export default index
