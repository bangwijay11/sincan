import {Image, Text, View} from 'react-native';

import { Pertolongan } from '../../property';
import React from 'react';
import {TouchableOpacity} from 'react-native';

const index = props => {
  return (
    <TouchableOpacity
      onPress={props.move}
      style={{
        width: 140,
        height: 200,
        backgroundColor: props.color,
        borderRadius: 15,
        marginTop: props.marginTop,
        marginRight: 20,
        shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
      }}>
      <View style={{alignItems: 'center'}}>
        <Text
          style={{
            color: 'white',
            fontSize: 14,
            marginTop: 22,
            textAlign: 'center',
          }}>
          {props.title}
        </Text>
        <Image
          source={props.Image}
          style={{width: 117, height: 117, marginTop: 10}}
        />        
      </View>
    </TouchableOpacity>
  );
};

export default index;
