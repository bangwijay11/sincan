import React from 'react';
import { View as RNView, ViewProps } from 'react-native';
import { AUTO_PATCH_STYLE } from '../app/Helpers';

class View extends React.Component<ViewProps> {
  render() {
    const { children, style } = this.props;
    const patchedStyle = AUTO_PATCH_STYLE(style);
    return (
      <RNView {...this.props} style={patchedStyle}>
        {children}
      </RNView>
    );
  }
}

export default View;
