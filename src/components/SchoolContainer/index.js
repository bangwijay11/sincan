import {Image, StyleSheet, Text, View} from 'react-native';

import { BottonLesson } from '..';
import {Colors} from '../../property/colors/colors';
import React from 'react';
import {TouchableOpacity} from 'react-native';

const index = props => {
  return (
    <TouchableOpacity onPress={props.move} style={styles.cardChapter}>
      <View style={styles.chapterContainer}>
        <View>
          <Text style={styles.textChapterTitle}>{props.title}</Text>
          <View style={styles.chapterDesc}>
            <Text style={styles.textChapterDesc}>{props.desc}</Text>
          </View>
        </View>
        <View style={styles.chapterImgContainer}>
          <Image source={props.Image} style={styles.chapterImg} />
        </View>
        
      </View>
    </TouchableOpacity>
  );
};

export default index;

const styles = StyleSheet.create({
  cardChapter: {
    width: 300,
    height: 110,
    backgroundColor: Colors.chapter,
    borderRadius: 15,
    marginTop: 15,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  chapterContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  chapterDesc: {
    width: 176,
    height: 30,
  },
  textChapterTitle: {
    color: Colors.textDefault,
    fontSize: 15,
    fontWeight: '700',
    marginTop: 10,
  },
  textChapterDesc: {
    fontSize: 12,
    color: '#FEFED5',
    marginTop: 15,
  },
  chapterImgContainer: {
    width: 90,
    height: 90,
  },
  chapterImg: {width: 90, height: 90},
});
