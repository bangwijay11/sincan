import { Image, Text, View } from 'react-native'

import React from 'react'
import { ScrollView } from 'react-native'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native'

const index = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.card} onPress={props.move}>
        <View style={styles.txtContainer}>
          <Text style={styles.judul}>{props.judul}</Text>
          <Text style={styles.desc}>{props.desc}</Text>
        </View>
        <View style={styles.imgContainer}>
          <Image style={styles.img} source={props.photo}/>
        </View>
      </TouchableOpacity>

    </View>
  )
}

export default index
const styles = StyleSheet.create({
  container: {
     marginTop: 20 
  },
  card: {
    width: 400, 
    height: 100, 
    backgroundColor: 'white', 
    flexDirection: 'row', 
    justifyContent: 'space-between' 
  },
  desc:{color:'grey'},
  img:{width: 100, height: 100},
  imgContainer:{
    width: 100, 
    height: 100, 
    paddingRight: 8, 
    justifyContent: 'center'
  },

  judul: {fontWeight: 'bold', fontSize: 20},
  txtContainer: {width: 200, padding: 10}
})