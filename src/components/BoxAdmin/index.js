import { Text, View } from 'react-native'

import { Colors } from '../../property/colors/colors'
import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native'

const index = ({title, move}) => {
    return (
        <View>
             <TouchableOpacity style={styles.box} onPress={move}>
                    <Text style={styles.txtBox}>{title}</Text>
                </TouchableOpacity>
        </View>
    )
}

export default index
const styles = StyleSheet.create({
    box:{
        width: 150, 
        height: 90,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primary,
        borderRadius: 25,
        shadowColor: "#000",
        marginBottom: 25,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        
        elevation: 12,
    },
    txtBox:{
        color: Colors.font,
        fontSize: 16,
        paddingHorizontal: 16,
        textAlign: 'center'
    }
})
