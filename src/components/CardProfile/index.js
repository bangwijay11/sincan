import { Text, View } from 'react-native'

import { Colors } from '../../property/colors/colors'
import React from 'react'
import { StyleSheet } from 'react-native'

const index = (props) => {
    return (
        <View>
        <View style={styles.nameContainer}>
        <Text style={styles.name}>{props.title}</Text>
        </View>
        </View>
    )
}

export default index
const styles = StyleSheet.create({
    nameContainer: {
        width: 300,
        height: 40,
        backgroundColor: Colors.primary,
        alignSelf: 'center',
        alignItems: 'center', justifyContent: 'center',
        margin: 10
    },
    name: {
        fontSize: 16,
        color: Colors.font
    },
})
