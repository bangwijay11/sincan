import React, { useState } from 'react';
import { SUBTITLE_COURSE, TITLE_COURSE } from '../FontStyle/FontStyle';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import Video from 'react-native-video';
import { useFocusEffect } from '@react-navigation/core';

const index = (props) => {
  const [paused, setPaused] = useState(true);
  const [text, setText] = useState('Play');

  console.log('paused', paused);

  const onPlay = () => {
    if (paused == true && text === 'Play') {
      setPaused(false);
      setTimeout(() => {
        setText('Paused');
      }, 1000);
    } else if (paused == false && text === 'Paused') {
      setPaused(true),
        setTimeout(() => {
          setText('Play');
        }, 1000);
    } else {
      setPaused(true);
    }
  };
  // useFocusEffect(() => {
  //   onPlay();
  // }, [paused]);
  return (
    <LinearGradient colors={['#74EBD5', '#ACB6E5']} style={styles.linear}>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.sroll}>
        <Video
          paused={paused}
          source={require('../../property/Pengertian_Gempa_Bumi.mp4')}
          resizeMode="contain"
          style={styles.video}
        />
        <TouchableOpacity onPress={onPlay}>
          <Text style={styles.play}>{text}</Text>
        </TouchableOpacity>
        <Text style={TITLE_COURSE}>{props.bab}</Text>
        <View style={styles.content}>
          <View>
            <Text style={SUBTITLE_COURSE}>{props.desc} </Text>
          </View>
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default index;
const styles = StyleSheet.create({
  linear: {
    width: '100%',
    borderRadius: 30,
    alignItems: 'center',
    marginBottom: 90,
  },
  sroll: { width: '90%', marginTop: 15, marginBottom: 40 },
  video: { height: 260, marginTop: 0, borderRadius: 23 },
  content: { marginTop: 8 },
  play: { textAlign: 'center', color: 'white', fontSize: 18 },
});
