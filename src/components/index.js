import Alert from './Alert/alert'
import BackButton from './BackButton';
import BarStatus from './Statusbar'
import BottonLesson from './ButtonLesson'
import BoxAdmin from './BoxAdmin'
import ButtonSign from './ButtonSign'
import CardChapter from './CardChapter';
import CardCourse from './CardCourse'
import CardHeaderClass from './CardHeaderClas'
import CardLesson from './CardLesson'
import CardMenu from './CardMenu';
import CardProfile from './CardProfile'
import CardSchool from './CardSchool'
import CardSubChapter from './CardSubChapter';
import DrawerContent from './DrawerContent/Container/DrawerContainer';
import FontStyle from './FontStyle/FontStyle'
import Input from './Input'
import InputText from './TextInput'
import Loading from './Loading'
import LoadingPage from './LoadingPage'
import NewsCard from './NewsCard'
import NewsCardV from './NewsCardV'
import RowItem from './RowItem'
import SchoplContainer from './SchoolContainer'
import SwiperContent from './SwiperContent'
import Swipper from './OnBoarding'

export { LoadingPage ,BoxAdmin,ButtonSign, DrawerContent, CardProfile ,Swipper ,SchoplContainer,Input,CardSchool ,CardCourse ,RowItem ,Alert ,BackButton ,InputText ,SwiperContent ,CardHeaderClass ,BottonLesson ,CardLesson ,NewsCardV,NewsCard ,Loading ,CardMenu, CardSubChapter, CardChapter,  FontStyle, BarStatus };
