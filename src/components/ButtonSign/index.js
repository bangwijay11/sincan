import { Text, View } from 'react-native'

import { Colors } from '../../property/colors/colors'
import React from 'react'
import { StyleSheet } from 'react-native'
import { TEXT_LOGIN } from '../FontStyle/FontStyle'
import { TouchableOpacity } from 'react-native'

const index = (props) => {
    return (
        <View>
            <TouchableOpacity onPress={props.onPress} disabled={props.disabled} style={styles.btnLogin}>
                <Text style={TEXT_LOGIN}>{props.title}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default index
const styles = StyleSheet.create({
    btnLogin: {
        width: 314,
        height: 42,
        borderRadius: 15,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 15
      },
})
