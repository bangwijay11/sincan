import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import Axios from 'axios';
import { Image } from 'react-native';
import { KOS_URL } from '@env';
import { Logout } from '../../../property';
import { TouchableOpacity } from 'react-native';

const DrawerComponent = (props) => {
  const { form } = useSelector((state) => state.LoginReducer);
  const dispatch = useDispatch();
  const { id } = useSelector((state) => state.LoginReducer);
  const [user, setUser] = useState();
  const [email, setEmail] = useState();

  const { username } = useSelector(
    (state) => state.ListProdukReducer.lisProduk,
  );

  return (
    <View style={{ flex: 1, paddingHorizontal: 18 }}>
      <View style={styles.ctnImg}>
        <Image
          source={{ uri: `https://ui-avatars.com/api/?name=${username}` }}
          style={{ width: 60, height: 60, borderRadius: 20 }}
        />
      </View>
      <View style={styles.ctnName}>
        <Text>{username}</Text>
      </View>

      <TouchableOpacity
        onPress={props.logout}
        style={{ marginTop: 50, flexDirection: 'row' }}
      >
        <View style={{ marginRight: 20, marginTop: 4 }}>
          <Logout />
        </View>
        <View>
          <Text>Logout</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default DrawerComponent;

const styles = StyleSheet.create({
  ctnImg: {
    marginTop: 50,
  },
  ctnName: {
    marginTop: 20,
  },
});
