import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';

import Axios from 'axios';
import DrawerComponent from '../Component/DrawerComponent';
import { KOS_URL } from '@env';
import { useSelector } from 'react-redux';
import auth from '@react-native-firebase/auth';

const DrawerContainer = (props) => {
  const { form } = useSelector((state) => state.LoginReducer);
  const { id } = useSelector((state) => state.LoginReducer);
  const [user, setUser] = useState();
  const [email, setEmail] = useState();
  const LogoutHandle = () => {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'));
    (form.username = ''), (form.password = '');
    props.navigation.navigate('Login');
  };
  const getUser = () => {
    Axios.get(`${KOS_URL}/users/${id}`).then((res) => {
      // console.log('sadsa', res.data);
      setUser(res.data.username);
      setEmail(res.data.email);
      // console.log('id', res.data.email);
      // console.log('form', form);
    });
  };
  useEffect(() => {
    getUser();
  });
  return (
    <DrawerComponent
      user={user}
      email={email}
      logout={LogoutHandle}
      {...props}
    />
  );
};

export default DrawerContainer;
