import { StatusBar, Text, View } from 'react-native'

import { Colors } from '../../property/colors/colors'
import React from 'react'

const index = () => {
    return (
        <StatusBar backgroundColor={Colors.statusbar}  barStyle="dark-content" />
    )
}

export default index
