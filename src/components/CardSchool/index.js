import React, {useState} from 'react'
import { SUBTITLE_COURSE, TITLE_COURSE } from '../FontStyle/FontStyle';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'

import { BottonLesson } from '..';
import LinearGradient from 'react-native-linear-gradient';
import { StyleSheet } from 'react-native';
import VideoPlayer from 'react-native-video-player';

const index = (props) => {
  return (
    <LinearGradient colors={['#74EBD5', '#ACB6E5']} style={styles.linear}>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
        <Text style={TITLE_COURSE}>{props.bab}</Text>
        <View style={styles.txt}>
          <View>
            <Text style={SUBTITLE_COURSE}>{props.desc} </Text>
          </View>
          {/* <View style={{ marginTop: 20 }}> */}
          {/* <BottonLesson title={props.titleBtn} move={props.onPress} /> */}
        {/* </View> */}
        </View>
      </ScrollView>
    </LinearGradient>
  )
}

export default index
const styles = StyleSheet.create({
  linear: {
     width: '100%',
    borderRadius: 30,
    alignItems: 'center',
    marginBottom: 0,
    paddingBottom: 20 
  },
  scroll: {
    width: '80%', 
    marginTop: 23, 
    marginBottom: 15 
  },
  txt: { marginTop: 8 }
})
