import {font} from '../../property/colors/colors'

export const FONT =  font

const FONT_SECONDARY_REGULAR = 'Quicksand-Regular';
const FONT_SECONDARY_MEDIUM = 'Quicksand-Medium';
const FONT_SECONDARY_LIGHT = 'Quicksand-Light';
const FONT_SECONDARY_PRIMARY = 'Quicksand-Bold';

const FONT_SIZE_TITLE = 32;
const FONT_SIZE_SUBTITLE = 16
const FONT_SIZE_INPUT = 14
const FONT_ONBOARDING = 20




export const TITLE_COURSE ={
    fontFamily: FONT_SECONDARY_PRIMARY,
    fontSize: FONT_SIZE_TITLE,
    color: '#FFFFFF', 
}

export const SUBTITLE_COURSE = {
    fontFamily: FONT_SECONDARY_MEDIUM,
    fontSize: FONT_SIZE_SUBTITLE,
    color: '#FFFFFF', 
}
export const TEXT_DONE = {
    fontFamily: FONT_SECONDARY_MEDIUM,
    fontSize: FONT_SIZE_SUBTITLE,
    color: '#FFFFFF', 
    textAlign: 'center'
}
export const TEXT_INPUT = {
    fontFamily: FONT_SECONDARY_MEDIUM,
    fontSize: FONT_SIZE_INPUT,
    color: '#000000',
}
export const TEXT_LOGIN = {
    fontFamily: FONT_SECONDARY_MEDIUM,
    fontSize: FONT_SIZE_INPUT,
    color: '#FFFFFF',
}
export const TEXT_ONBOARDING = {
    fontFamily: FONT_SECONDARY_MEDIUM,
    fontSize: FONT_ONBOARDING,
    color: '#434242',
}


