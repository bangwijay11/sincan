import { Image, Text, TouchableOpacity, View } from 'react-native'

import React from 'react'

const index = (props) => {
    return (
        <TouchableOpacity onPress={props.move} style={{width: 350, height: 100, marginBottom: 10}}>
            <Image source={props.img} style={{width: '100%', height: '100%',borderRadius: 15, position: 'relative', resizeMode: 'stretch'}} />
            <View style={{position: 'absolute', width: 206,  bottom: 15, marginLeft: 18}}>
                <Text style={{color: 'white', fontWeight: 'bold', fontSize: 15}}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default index
