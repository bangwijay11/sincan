import {Image, StyleSheet, Text, View} from 'react-native';

import React from 'react';
import {TouchableOpacity} from 'react-native';

const index = props => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.card}>
        {/* text&Button */}
        <View>
          <View style={styles.textContainer}>
            <Text style={styles.title}> {props.title} </Text>
          </View>
          <View style={styles.infoContainer}>
            <TouchableOpacity style={styles.box} onPress={props.move}>
              <Text style={styles.textBox}>Pelajari</Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* img */}
        <View style={styles.imgContainer}>
          <View style={styles.img}>
            <Image source={props.imgBgBencana} style={styles.imgBgBencana} />
          </View>
          <View style={styles.imgBencana}>
            <Image source={props.imgBencana} style={styles.icon} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default index;
const styles = StyleSheet.create({
  cardContainer: {
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 10,
  },
  textContainer: {
    width: 125,
    marginTop: 36,
    marginLeft: 20,
  },
  card: {
    width: 320,
    height: 136,
    backgroundColor: '#F0F0F8',
    borderRadius: 10,
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
  infoContainer: {
    marginLeft: 20,
    marginTop: 20,
  },
  box: {
    width: 100,
    height: 42,
    backgroundColor: '#F76770',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBox: {color: 'white', fontWeight: '500', fontSize: 14},
  imgContainer: {marginTop: 27, marginLeft: 47},
  img: {
    width: 128,
    height: 110,
    position: 'relative',
  },
  imgBgBencana: {width: '100%', height: '100%'},
  imgBencana: {position: 'absolute'},
  icon: {width: 100, height: 100},
});
