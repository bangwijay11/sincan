import { Text, View } from 'react-native'

import React from 'react'
import { TouchableOpacity } from 'react-native'

const index = (props) => {
    return (
        <TouchableOpacity onPress={props.move} style={{width: 150,  alignSelf: 'center' ,height: 42, backgroundColor: '#32DBC6', borderRadius: 30, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{color: 'white'}}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default index
