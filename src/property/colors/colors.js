export const Colors = {
    background : '#F6F6F6',
    primary : '#6FC9C0',
    font: '#FEFED5',
    error: '#D0021B',
    statusbar: '#FFFFFF',
    textDefault: '#FEFED5',
    chapter: '#38ADE2',
  };
  