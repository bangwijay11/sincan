import BackIcon from './illustration/icon/Back.svg'
import Bottom_elips from './illustration/illustration//Bottom_ellips.svg'
import Help from './illustration/illustration/help.svg'
import Home from './illustration/icon/Home.svg'
import Lesson from './illustration/illustration/lesson.svg'
import Logo from './illustration/logo/Logo.svg'
import LogoSincan from './illustration/logo/LogoSincan.svg'
import Logout from './illustration/icon/Logout.svg'
import NewsLetter from './illustration/illustration/newspaper.svg'
import Onboarding from './illustration/illustration/OB.svg'
import Pertolongan from './illustration/illustration/SiagaPertolongan.svg'
import Sincan from './illustration/logo/Sincan.svg'

export {Logo, Lesson ,NewsLetter ,Help ,Onboarding, Bottom_elips, Sincan, Home, BackIcon, Pertolongan, LogoSincan, Logout}