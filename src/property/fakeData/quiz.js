export const quiz = [
    {
      id: 1,
      question: "Apa yang dimaksud Gempa?",
      options: [
        {
          title: " getaran yang terjadi dibumi karna sebab tertentu",
          correct: true,
        },
        {
          title: "Air yang meluap",
          correct: false,
        },
        {
          title: "Pergantian musim",
          correct: false,
        },
        {
          title: "Air dari laut yang datang dengan kecepatan tinggi",
          correct: false,
        },
      ],
    },
    {
      id: 2,
      question: "Apakah contoh penyebab terjadinya gempa",
      options: [
        {
          title: "Hujan terus menerus",
          correct: false,
        },
        {
          title: "adanya gelombang sesmitik",
          correct: true,
        },
        {
          title: "Angin yang kencang",
          correct: false,
        },
      ],
    },
    {
      id: 3,
      question: "Apa Yang dimaksud tanah longsor",
      options: [
        {
          title: "Air yang meluap",
          correct: false,
        },
        {
          title: "suatu peristiwa geologi yang terjadi karena pergerakan masa batuan ",
          correct: true,
        },
        {
          title: "Keluarnya larva dari gunung",
          correct: false,
        },
      ],
    },
    {
      id: 4,
      question: "Apa contoh faktor pemicu tanah longsor",
      options: [
        {
          title: "Faktor dari ulah manusia",
          correct: false,
        },
        {
          title: "faktor yang menyebabkan bergeraknya material tersebut",
          correct: true,
        },
        {
          title: "akibat pencemaran udara",
          correct: false,
        },
      ],
    },
    {
      id: 5,
      question: "Faktor Pendorong Tanah LOngsor",
      options: [
        {
          title: "Akibat pencemaran air",
          correct: false,
        },
        {
          title: "faktor-faktor yang memengaruhi kondisi material sendiri",
          correct: true,
        },
        {
          title: "Akibat banjir",
          correct: false,
        },
      ],
    },
    
  ];