
/* eslint-disable prettier/prettier */
const initialStateLogin = {
    form: {
      username: '',
      password: '',
    },
    id: '',
    isLoggin: false,
    title: 'Login Page',
    desc: 'Masukkan username & password anda',
  };

  const LoginReducer = (state = initialStateLogin, action) => {
    const newState = Object.assign({}, state)
      console.log(action);
      switch (action.type) {
        case 'SET_FORM_LOGIN':
          return {
            ...state,
            form: {
              ...state.form,
              [action.inputType]: action.inputValue,
            },
          };
        case 'LOGIN_USER': 
        return {
          ...newState,
          
        };
        case 'LOGOUT_USER': 
        return {
          ...state.isLoggin = false
        };
        case 'SET_USER_ID':
          return {
            ...state,
            id: action.payload
          }
      }
      return state;
  };

  export default LoginReducer;

