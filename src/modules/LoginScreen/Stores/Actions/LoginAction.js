export const setFormLogin = (inputType, value) => {
    return {type: 'SET_FORM_LOGIN', inputType: inputType, inputValue: value};
  };
export const submitlogin = (data) => {
    return {type: 'LOGIN_USER', payload: data}
};

export const setIdUser = id => {
  return{
    type: 'SET_USER_ID',
    payload: id
  }
}

export const logout = () => {
  return {type: 'LOGOUT_USER'}
}