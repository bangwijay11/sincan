import * as Animatable from 'react-native-animatable';

import { BarStatus, ButtonSign } from '../../../components';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
} from 'react-native';
import React, { useEffect, useState } from 'react';

import { Colors } from '../../../property/colors/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAvoidingView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import NetInfo from '@react-native-community/netinfo';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native';
import { StatusBar } from 'react-native';
import { TEXT_INPUT } from '../../../components/FontStyle/FontStyle';
import images from '../../../app/config/images';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { setProfileName } from '../../HomeScreen/Stores/Actions/HomeActions';

GoogleSignin.configure({
  webClientId:
    '373474965583-7f8ta7529ocnos43jkcm4vd2dj57brqq.apps.googleusercontent.com',
});

const LoginScreen = (props) => {
  const [enableShift, setenableShift] = useState(false);
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const dispatch = useDispatch();

  console.log('userrrrScreenr', user);

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    dispatch(setProfileName(user?.displayName));
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  async function onGoogleButtonPress() {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <StatusBar translucent backgroundColor="transparent" />
      <View style={styles.elipsContainer}>
        <LinearGradient colors={['#344A9D', '#60B6AD']} style={styles.elips} />
      </View>
      <KeyboardAvoidingView
        enabled={enableShift}
        behavior="position"
        style={styles.FlexGrowOne}
      >
        <View style={styles.logoContainer}>
          <Image source={images.logo} style={styles.logo} />
        </View>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.formCOntainer}>
            <Text style={styles.text_label}>Email</Text>
            <View style={styles.action}>
              <FontAwesome name="user" color={Colors.primary} size={20} />
              <TextInput
                placeholder="Your Email"
                placeholderTextColor="#BDBDBD"
                style={[TEXT_INPUT, styles.textInput]}
                value={props.valueName}
                autoCapitalize="none"
                onFocus={() => setenableShift(true)}
                onChangeText={props.InputName}
                onEndEditing={props.onEndEditing}
              />
              {props.animName}
            </View>
            {props.errName}
            <Text
              style={[
                styles.text_label,
                {
                  marginTop: 25,
                },
              ]}
            >
              Password
            </Text>
            <View style={styles.action}>
              <FontAwesome name="lock" color={Colors.primary} size={20} />
              <TextInput
                placeholder="Your Password"
                placeholderTextColor="#BDBDBD"
                secureTextEntry={props.secureTextEntry}
                style={[TEXT_INPUT, styles.textInput]}
                value={props.valuePass}
                autoCapitalize="none"
                onFocus={() => setenableShift(true)}
                onChangeText={props.changePass}
              />
              <TouchableOpacity onPress={props.security}>
                {props.showPass}
              </TouchableOpacity>
            </View>
            {props.errPass}
          </View>
          <View style={styles.btnLoginContainer}>
            <ButtonSign
              title="Masuk"
              onPress={props.LoginHandle}
              disabled={props.disabled}
            />
            <GoogleSigninButton
              title="Google Sign-In"
              style={{ width: 314 }}
              color={GoogleSigninButton.Color.Light}
              onPress={() =>
                onGoogleButtonPress().then(
                  () => props.navigation.replace('Home'),
                  // console.log('halo'),
                )
              }
            />
          </View>
          <View style={styles.btnRegisterContainer}>
            <View>
              <Text style={styles.textAkun}>Belum Punya Akun?</Text>
            </View>
            <TouchableOpacity style={styles.btnReg} onPress={props.regist}>
              <Text style={styles.textRegister}>Daftar</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.background,
    flex: 1,
  },
  elipsContainer: { alignItems: 'flex-end' },
  elips: { width: 110, height: 110, borderBottomLeftRadius: 200 },
  logoContainer: { alignItems: 'center' },
  logo: { width: 200, height: 70 },
  formCOntainer: {
    marginTop: 40,
    paddingHorizontal: 20,
  },

  text_label: {
    color: '#BDBDBD',
    fontSize: 14,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#BDBDBD',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
  },

  btnLogin: {
    width: 314,
    height: 42,
    borderRadius: 15,
    backgroundColor: Colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnLoginContainer: {
    alignItems: 'center',
    marginTop: 30,
  },
  btnRegisterContainer: {
    marginTop: 15,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  textAkun: { color: '#8C8C8C' },
  btnReg: { marginLeft: 8 },
  textRegister: { color: Colors.primary },
  FlexGrowOne: {
    flexGrow: 1,
  },
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    top: 30,
  },
  offlineText: { color: '#fff' },
});
