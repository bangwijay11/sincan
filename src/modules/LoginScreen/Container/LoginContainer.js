import * as Animatable from 'react-native-animatable';

import React, { useEffect, useState } from 'react';
import {
  setFormLogin,
  setIdUser,
  submitlogin,
} from '../Stores/Actions/LoginAction';
import { useDispatch, useSelector } from 'react-redux';

import { Alert } from 'react-native';
import Axios from 'axios';
import Feather from 'react-native-vector-icons/Feather';
import firestore from '@react-native-firebase/firestore';
import LoginScreen from '../Component/LoginScreen';
import { StyleSheet, Text, ToastAndroid } from 'react-native';
import auth from '@react-native-firebase/auth';
import { ActivityIndicator } from 'react-native';
import { getFirstWordName } from '../../../app/Helpers';
import { setProfileName } from '../../HomeScreen/Stores/Actions/HomeActions';

const LoginContainer = (props) => {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const { form, id } = useSelector((state) => state.LoginReducer);
  const { username } = useSelector(
    (state) => state.ListProdukReducer.lisProduk,
  );
  const dataUser = { form }.form;
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  // if (user !== null) {
  //   props.navigation.navigate('Home');
  // }

  console.log('userrrrrr', user);

  console.log('userrrrryr', username);
  // Handle user state changes
  const onAuthStateChanged = (user) => {
    setUser(user);
    if (initializing) setInitializing(false);
  };
  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);
  const userData0 = { form }.form;

  const getUser = async () => {
    await firestore()
      .collection('users')
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          const data = doc.data();
          console.log(
            'sasasasa',
            doc.id,
            data.email === userData0.username
              ? dispatch(setProfileName(data.name))
              : console.log('wlwllw'),
          );
        });
      })
      .catch((err) => {
        console.log('Error getting documents', err);
      });
  };
  const Login = () => {
    auth()
      .signInWithEmailAndPassword(
        { form }.form.username,
        { form }.form.password,
      )
      .then(() => {
        if (dataUser.username === 'Admin1@gmail.com') {
          props.navigation.navigate('Admin');
        } else {
          getUser();
          props.navigation.navigate('Home');
        }
      })
      .catch((error) => {
        ToastAndroid.show(error.code, ToastAndroid.SHORT);
        // console.error(error);
      });
  };
  const [data, setData] = useState({
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
    disableButton: true,
  });
  const LoginAdmin = () => {
    console.log('login');
  };
  const textInputChange = () => {
    if ({ form }.form.username.trim().length !== 0) {
      setData({
        ...data,
        check_textInputChange: true,
        isValidUser: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        check_textInputChange: false,
        isValidUser: false,
        disableButton: true,
      });
    }
  };
  const onInputChange = (value, inputType) => {
    if (inputType === 'username') {
      textInputChange();
    } else {
      handlePasswordChange();
    }
    dispatch(setFormLogin(inputType, value));
  };
  const handlePasswordChange = () => {
    if ({ form }.form.password.trim().length >= 7) {
      setData({
        ...data,
        isValidPassword: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        isValidPassword: false,
        disableButton: true,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = () => {
    if ({ form }.form.username.trim().length !== 0) {
      setData({
        ...data,
        isValidUser: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
        disableButton: true,
      });
    }
  };
  const RegistHandle = () => {
    props.navigation.navigate('Register');
    (form.username = ''),
      (form.email = ''),
      (form.password = ''),
      setData({
        data,
      });
  };

  return (
    <LoginScreen
      data
      valueName={{ form }.form.username}
      onEditing={(e) => handleValidUser(e.nativeEvent.text)}
      InputName={(value) => onInputChange(value, 'username')}
      animName={
        data.check_textInputChange ? (
          <Animatable.View animation="bounceIn">
            <Feather name="check-circle" color="green" size={20} />
          </Animatable.View>
        ) : null
      }
      errName={
        data.isValidUser ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>
              Nama Pengguna Tidak Boleh Kosong.
            </Text>
          </Animatable.View>
        )
      }
      valuePass={{ form }.form.password}
      changePass={(value) => onInputChange(value, 'password')}
      secureTextEntry={data.secureTextEntry ? true : false}
      security={updateSecureTextEntry}
      showPass={
        data.secureTextEntry ? (
          <Feather name="eye-off" color="grey" size={20} />
        ) : (
          <Feather name="eye" color="grey" size={20} />
        )
      }
      errPass={
        data.isValidPassword ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>
              Password must be 8 characters long.
            </Text>
          </Animatable.View>
        )
      }
      LoginHandle={Login}
      LoginAdmin={LoginAdmin}
      regist={RegistHandle}
      disabled={data.disableButton}
      {...props}
    />
  );
};

export default LoginContainer;
const styles = StyleSheet.create({
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
});
