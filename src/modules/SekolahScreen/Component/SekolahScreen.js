import {
  BarStatus,
  CardHeaderClass,
  SchoplContainer,
} from '../../../components';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ActivityIndicator } from 'react-native';
import { getMateri } from '../Stores/Actions/SekolahAction';
import images from '../../../app/config/images';

const SekolahScreen = (props) => {
  const SekolahReducer = require('../Component/sekolah.json');
  return (
    <ImageBackground source={images.bg_course} style={styles.container}>
      <BarStatus />
      <CardHeaderClass title="Sekolah Siaga Bencana" img={images.sekolah} />
      {SekolahReducer.map((SekolahReducer) => (
        <View key={SekolahReducer.id}>
          <SchoplContainer
            move={() =>
              props.navigation.navigate('CSchool', {
                judul: SekolahReducer.judul,
                isi: SekolahReducer.isi,
              })
            }
            title={SekolahReducer.judul}
            desc={SekolahReducer.desc}
            Image={images.sekolah}
          />
        </View>
      ))}
    </ImageBackground>
  );
};

export default SekolahScreen;

const styles = StyleSheet.create({
  ActivityIndicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    flex: 1,
  },
});
