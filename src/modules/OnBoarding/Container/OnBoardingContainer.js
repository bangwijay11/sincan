import { Text, View } from 'react-native'

import OnBoarding from '../Component/OnBoarding'
import React from 'react'

const OnBoardingContainer = (props) => {
    return (
        <OnBoarding
            move={() => props.navigation.replace('Login')}
            {...props}
        />
    )
}

export default OnBoardingContainer
