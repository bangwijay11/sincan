import { Help, Lesson, NewsLetter } from '../../../property';
import { StyleSheet, Text, View } from 'react-native';

import { Colors } from '../../../property/colors/colors';
import Onboarding from 'react-native-onboarding-swiper';
import React from 'react';
import { StatusBar } from 'react-native';
import { TouchableOpacity } from 'react-native';

const Dots = ({ selected }) => {
  let backgroundColor;

  backgroundColor = selected ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.3)';

  return (
    <View
      style={{
        width: 6,
        height: 6,
        marginHorizontal: 3,
        backgroundColor,
      }}
    />
  );
};
const Skip = ({ ...props }) => (
  <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
    <Text style={{ fontSize: 16 }}>Skip</Text>
  </TouchableOpacity>
);
const Next = ({ ...props }) => (
  <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
    <Text style={{ fontSize: 16 }}>Next</Text>
  </TouchableOpacity>
);

const Done = ({ ...props }) => (
  <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
    <Text style={{ fontSize: 16 }}>Done</Text>
  </TouchableOpacity>
);
const OnBoarding = ({ navigation }) => {
  return (
    <>
      <StatusBar translucent backgroundColor="transparent" />
      <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => navigation.replace('Login')}
        onDone={() => navigation.replace('Login')}
        pages={[
          {
            backgroundColor: Colors.background,
            image: <Lesson width={350} height={300} />,
            titleStyles: { color: 'black' },
            title: 'Mulai Belajar',
            subtitle:
              'Mulailah belajar dengan pembelajaran yang telah kami sediakan',
          },
          {
            backgroundColor: Colors.background,
            image: <Help width={500} height={300} />,
            titleStyles: { color: 'black' },
            subTitleStyles: { color: 'black' },
            title: 'Selamatkan Diri',
            subtitle:
              'Lakukan Panggilan darurat jika terjadi bencana dan gunakan maps untuk mencari tempat yang lebih aman',
          },
          {
            backgroundColor: Colors.background,
            image: <NewsLetter width={500} height={300} />,
            titleStyles: { color: 'black' },
            subTitleStyles: { color: 'black' },
            title: 'apatkan Bertia Terbaru',
            subtitle: 'Dapatkan Berita terbaru tentang bencana alam',
          },
        ]}
      />
    </>
  );
};

export default OnBoarding;
