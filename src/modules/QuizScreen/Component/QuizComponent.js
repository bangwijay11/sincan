import React, { useEffect, useState } from 'react';
import { Text } from 'react-native';

import { ActivityIndicator } from 'react-native';
import { Alert } from 'react-native';
import { BackIcon } from '../../../property';
import { StatusBar } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { quiz as quizData } from '../../../property/fakeData/quiz';
import View from '../../../components/View';

const QuizComponent = (props) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [loading, setLoading] = useState(true);
  const [quiz, setQuiz] = useState(quizData);
  const [score, setScore] = useState({
    correct: 0,
    false: 0,
  });
  const { id, question, options } = quiz[currentIndex];

  const checkScore = () => {
    const questionAnswered = quiz.filter((item) => item.selected);
    const questionCorrect = questionAnswered.filter((item) =>
      item.options.find(
        (option) => option.correct && option.selected === option.correct,
      ),
    );
    setScore({
      correct: questionCorrect.length,
      false: quiz.length - questionCorrect.length,
    });
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
    checkScore();
  }, [quiz]);

  const nextQuestion = () => {
    if (quiz.length - 1 === currentIndex) return;
    setCurrentIndex(currentIndex + 1);
  };

  const previousQuestion = () => {
    if (currentIndex === 0) return;
    setCurrentIndex(currentIndex - 1);
  };
  const selectOption = (indexSelected, indexOptionSelected) => {
    setQuiz(
      quiz.map((item, index) =>
        index === indexSelected
          ? {
              ...item,
              selected: true,
              options: options.map((item, index) =>
                index === indexOptionSelected
                  ? { ...item, selected: true }
                  : { ...item, selected: false },
              ),
            }
          : item,
      ),
    );
  };
  const result = 20 * score.correct;
  return loading ? (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ActivityIndicator animating={loading} color="#00f0ff" />
      <Text style={{ marginTop: 10 }} children="Mohon tunggu sebentar" />
    </View>
  ) : (
    <View>
      <StatusBar backgroundColor="transparent" translucent />
      <View
        style={{
          width: '100%',
          backgroundColor: '#6FC9C0',
          height: 80,
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 18,
        }}
      >
        <View>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <BackIcon width={24} height={24} />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
          }}
        >
          Quiz
        </Text>
        <View
          style={{
            width: 50,
            height: 50,
            borderRadius: 25,
            backgroundColor: 'grey',
            marginTop: 15,
          }}
        ></View>
      </View>
      <View style={{ paddingHorizontal: 18, marginTop: 20 }}>
        <View
          style={{
            width: '100%',
            height: 170,
            backgroundColor: '#6FC9C0',
            borderRadius: 15,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text style={{ color: 'white', fontSize: 15 }}>
            {currentIndex + 1}. {question}
          </Text>
        </View>
        <View>
          {options.map((item, index) => (
            <View
              key={index}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                marginTop: 20,
              }}
            >
              <TouchableOpacity
                onPress={() => selectOption(currentIndex, index)}
                style={{
                  width: '95%',
                  padding: 10,
                  backgroundColor: item?.selected ? '#6FC9C0' : 'white',
                  marginRight: 5,
                  borderRadius: 15,
                  alignItems: 'center',
                  justifyContent: 'center',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 1,
                  },
                  shadowOpacity: 0.22,
                  shadowRadius: 2.22,

                  elevation: 3,
                }}
              >
                <Text
                  style={{
                    color: item?.selected ? 'white' : '#6FC9C0',
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}
                >
                  {item.title}
                </Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
        <View
          style={{
            justifyContent: 'space-between',
            display: 'flex',
            flexDirection: 'row',
            marginTop: 20,
            paddingHorizontal: 18,
          }}
        >
          <TouchableOpacity
            style={{
              width: 142,
              backgroundColor: '#6FC9C0',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 15,
              borderRadius: 15,
              alignItems: 'center',
              justifyContent: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.22,
              shadowRadius: 2.22,

              elevation: 3,
            }}
            onPress={() => previousQuestion()}
            disabled={currentIndex === 0 ? true : false}
          >
            <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
              Kambali
            </Text>
          </TouchableOpacity>
          {quiz.length - 1 === currentIndex ? (
            <TouchableOpacity
              style={{
                width: 142,
                backgroundColor: 'green',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 15,
                borderRadius: 15,
                alignItems: 'center',
                justifyContent: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}
              onPress={() => {
                /* 1. Navigate to the Details route with params */
                props.navigation.navigate('Result', {
                  result: result,
                  correct: score.correct,
                  incorrect: score.false,
                });
              }}
            >
              <Text
                style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
              >
                Finish
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{
                width: 142,
                backgroundColor: '#6FC9C0',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 15,
                borderRadius: 15,
                alignItems: 'center',
                justifyContent: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}
              onPress={() => nextQuestion()}
              // disabled={currentIndex === 0 ? true : false}
            >
              <Text
                style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
              >
                Selanjutnya
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

export default QuizComponent;
