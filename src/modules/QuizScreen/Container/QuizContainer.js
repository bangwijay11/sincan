import { Text, View } from 'react-native'

import QuizComponent from '../Component/QuizComponent'
import React from 'react'

const QuizContainer = (props) => {
    return (
        <QuizComponent {...props}/>
    )
}

export default QuizContainer
