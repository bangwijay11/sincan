import { BackButton, CardCourse, CardLesson, FontStyle } from '../../../components';
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

import { BackIcon } from '../../../property';
import React from 'react'
import { StatusBar } from 'react-native';
import images from '../../../app/config/images';

const CHelpScreen = (props) => {
  const { isi, Judul} = props.route.params
    return (
        <ImageBackground
          source={images.bg_subcourse}
          style={styles.container}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          <BackButton/>
          <View style={styles.imgContainer}>
            <Image
              source={images.logo}
              style={styles.imgLogo}
            />
          </View>
          <View style={styles.contentContainer}>
          <CardCourse bab={Judul} 
          desc={isi}
          titleBtn="Lanjutkan" onPress={() => props.navigation.navigate('Done')} />
          </View>
         
        </ImageBackground>
      );
}

export default CHelpScreen
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    back: {
      marginTop: 24,
      marginLeft: 20,
    },
    iconBack: {marginTop: 20, marginLeft: 20},
    imgContainer: {width: 185, height: 63, alignSelf: 'center'},
    contentContainer: {alignItems: 'center', marginTop: 63, paddingHorizontal:35 },
    imgLogo: {width: '100%', height: '100%'}
  });
  
