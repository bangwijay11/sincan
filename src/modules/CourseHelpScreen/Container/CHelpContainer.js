import { Text, View } from 'react-native'

import CHelpScreen from '../Component/CHelpScreen'
import React from 'react'

const CHelpContainer = (props) => {
    return (
        <CHelpScreen {...props}/>
    )
}

export default CHelpContainer
