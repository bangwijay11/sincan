import { Text, View } from 'react-native'

import CourseScreen from '../Component/CourseScreen'
import React from 'react'

const CourseContainer = (props) => {
    return (
        <CourseScreen {...props}/>
    )
}

export default CourseContainer
