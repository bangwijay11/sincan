import { BackButton, BarStatus, CardLesson } from '../../../components';
import { Image, ImageBackground, StyleSheet, View } from 'react-native'
import React, { useEffect, useState } from 'react'

import Axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';
import { Text } from 'react-native';
import images from '../../../app/config/images';

const CourseScreen = (props) => {
 
  const {bab, isi} = props.route.params
    return (
        <ImageBackground
          source={images.bg_lesson}
          style={styles.container}>
          <BarStatus/>
          <BackButton/>
          <View style={styles.imgContainer}>
            <Image
              source={images.logo}
              style={styles.imgLogo}
            />
          </View>
          <View style={styles.contentContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
          <CardLesson bab={bab} desc={isi}/>
          </ScrollView>
          </View>
          
           
         
        </ImageBackground>

      );
}

export default CourseScreen
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    back: {
      marginTop: 24,
      marginLeft: 20,
    },
    iconBack: {marginTop: 20, marginLeft: 20},
    imgContainer: {width: 185, height: 63, alignSelf: 'center'},
    contentContainer: {alignItems: 'center', marginTop: 50, marginBottom: 100},
    imgLogo: {width: '100%', height: '100%'}
  });
  
