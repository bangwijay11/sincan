import { BackButton, CardSubChapter } from '../../../components';
import {Button, StyleSheet, Text, View} from 'react-native';
import {
  Image,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import React, { useEffect, useState } from 'react';

import Axios from 'axios'
import { BackIcon } from '../../../property';
import {ScrollView} from 'react-native';

const SubClassScreen = props => {
  const [materi, setMateri] = useState([]);
  const getData = () => {
    Axios.get('http://192.168.0.8:3000/materi/')
    .then((res) => {
      setMateri(res.data);
        console.log('res data Materi : ', res.data);
    })
    .catch(err => console.log('err0r : ', err));
  };

  useEffect(() => {
    getData();
  }, []); 
  const {bab, judul, isi} = props.route.params
  return (
    <ImageBackground
      source={require('../../../property/images/bgsubcourse.png')}
      style={styles.container}>
      <StatusBar backgroundColor="white" />
     <BackButton/>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginRight: 31,
        }}>
        <View style={{marginLeft: 60, marginTop: 26}}>
          <Image
            source={require('../../../property/images/earthquake.png')}
            style={{width: 100, height: 100}}
          />
        </View>
        
            
       <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 24,
              fontWeight: '700',
              color: 'white',
              fontFamily: 'Quicksand',
              marginRight: 20,
            }}>
            {bab}
          </Text>
        </View>
        
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {materi.map((materi) => (
          <View key={materi.id}>
        <CardSubChapter 
          move={() => props.navigation.navigate('Course', {
            bab: bab,
            isi: isi
          })}
          title={judul}
          
          imgBgBencana={require('../../../property/images/Intersect.png')}
          imgBencana={require('../../../property/images/earthquake2.png')}
        />
    
      
          </View>
        ))}

      </ScrollView>
    </ImageBackground>
  );
};

export default SubClassScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  back: {
    marginTop: 24,
    marginLeft: 20,
  },

  title: {
    color: 'black',
    fontFamily: 'Quicksand',
    fontSize: 14,
    fontWeight: '700',
    fontStyle: 'normal',
  },
});
