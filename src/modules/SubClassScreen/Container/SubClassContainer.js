import { Text, View } from 'react-native'

import React from 'react'
import SubClassScreen from '../Component/SubClassScreen'

const SubClassContainer = (props) => {
    return (
        <SubClassScreen {...props}/>
    )
}

export default SubClassContainer
