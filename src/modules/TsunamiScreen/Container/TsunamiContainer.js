import { Text, View } from 'react-native'

import React from 'react'
import TsunamiComponent from '../Component/TsunamiComponent'

const TsunamiContainer = (props) => {
    return (
        <TsunamiComponent {...props} />
    )
}

export default TsunamiContainer
