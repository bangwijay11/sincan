import { BackButton, BarStatus, CardLesson, FontStyle } from '../../../components';
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'

import Axios from 'axios'
import images from '../../../app/config/images';

const TsunamiComponent = () => {
    const [materi, setMateri] = useState('')

    useEffect(()=> {
        Axios.get('http://192.168.0.4:3000/materi/2')
        .then(res => {
            setMateri(res.data);
            console.log('isi materi :', res.data)
          })
          .catch(err => {
            console.log(err)
          })
    })
    return (
    <ImageBackground
        source={images.bg_lesson}
        style={styles.container}>
        <BarStatus/>
        <BackButton/>
        <View style={styles.imgContainer}>
          <Image
            source={images.logo}
            style={styles.imgLogo}
          />
        </View>
        <View style={styles.contentContainer}>
        <CardLesson bab={materi.bab} subBab="A. Faktor Alam" desc={materi.isi} titleBtn="Lanjutkan" onPress={() => props.navigation.navigate('Done')} />
        </View>
        
         
       
      </ImageBackground>
    )
}

export default TsunamiComponent
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    back: {
      marginTop: 24,
      marginLeft: 20,
    },
    iconBack: {marginTop: 20, marginLeft: 20},
    imgContainer: {width: 185, height: 63, alignSelf: 'center'},
    contentContainer: {alignItems: 'center', marginTop: 63, paddingHorizontal:35 },
    imgLogo: {width: '100%', height: '100%'}
  });
