import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import React, { useEffect, useState } from 'react';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Geolocation from '@react-native-community/geolocation';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Image } from 'react-native';
import { TextInput } from 'react-native';

const initialState = {
  latitude: null,
  longitude: null,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};
const INITIAL_MARKER = [
  {
    id: 1,
    region: { latitude: -6.197179, longitude: 106.8346301 },
    title: 'UDIN HOSPITAL',
    desc: 'Telp.(0821)88990',
    urlImg:
      'https://mmc.tirto.id/image/otf/500x0/2020/03/03/rs-mitra-keluarga-mitrakeluarga.com_ratio-16x9.jpg',
  },
  {
    id: 2,
    region: { latitude: -6.1831875, longitude: 106.7991529 },
    title: 'RS. FATIMAH AZZAHRA',
    desc: 'Telp.(0821)8899990',
    urlImg:
      'https://primayahospital.com/wp-content/uploads/2018/04/Bekasi-timur-1.jpg',
  },
  {
    id: 3,
    region: { latitude: -6.187233, longitude: 106.8060391 },
    title: 'Primary Hospital',
    desc: 'Telp.(0821)8899990',
    urlImg:
      'https://primayahospital.com/wp-content/uploads/2018/04/Bekasi-timur-1.jpg',
  },
];
const MapScreen = () => {
  const [currentPosition, setCurentPosition] = useState(initialState);
  let myMap;

  useEffect(() => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { longitude, latitude } = position.coords;
        setCurentPosition({
          ...currentPosition,
          latitude,
          longitude,
        });
      },
      (error) => alert(error.message),
      { timeout: 20000, maximumAge: 1000 },
    );
  }, []);

  const renderMarker = () => {
    return INITIAL_MARKER.map((marker) => (
      <Marker
        key={marker.id}
        coordinate={marker['region']}
        title={marker.title}
        description={marker['desc']}
        onPress={() => {
          myMap.fitToCoordinates([marker['region']], {
            edgePadding: { top: 10, bottom: 10, left: 10, right: 10 },
            animated: true,
          });
        }}
      />
    ));
  };

  return currentPosition.latitude ? (
    <View style={{ flex: 1 }}>
      <StatusBar translucent backgroundColor="transparent" />
      <MapView
        ref={(ref) => (myMap = ref)}
        showsUserLocation
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={currentPosition}
      >
        {renderMarker()}
      </MapView>
    </View>
  ) : (
    <ActivityIndicator style={{ flex: 1 }} animating size="large" />
  );
};

export default MapScreen;

const styles = StyleSheet.create({
  map: {
    height: '100%',
    position: 'relative',
  },
  auto: {
    position: 'absolute',
    marginTop: 90,
  },
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  bubble: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    backgroundColor: 'white',
    borderRadius: 6,
  },
  name: {
    // fontSize: 10,
    textAlign: 'center',
  },
});

{
  /* <Callout tooltip>
            <View>
                <View style={styles.bubble}>
                    <Text style={styles.name}>Kamu disini</Text>
                    <Image source={require('../../../property/images/danger.png')} />
                </View>
            </View>
        </Callout> */
}
