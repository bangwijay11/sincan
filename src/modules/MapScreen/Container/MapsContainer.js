import { Text, View } from 'react-native'

import MapScreen from '../Component/MapScreen'
import React from 'react'

const MapsContainer = (props) => {
    return (
        <MapScreen {...props}/>
    )
}

export default MapsContainer
