import {
  Alert,
  Button,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  deleteMateri,
  postMateri,
} from '../../AMateriScreen/Stores/Actions/AMateriAction';
import {deleteNomor, postNomor} from '../Stores/Actions/ANumberAction';
import {useDispatch, useSelector} from 'react-redux';

import Axios from 'axios';
import {KOS_URL} from '@env';
import images from '../../../app/config/images';

const VItem = ({name, alamat, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Image source={images.sincan} style={styles.avatar} />
      </TouchableOpacity>
      <View style={styles.alamat}>
        <Text style={styles.descName}>{name}</Text>
        <Text style={styles.descBidang}>{alamat}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>x</Text>
      </TouchableOpacity>
    </View>
  );
};
const ANumberComponent = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [telp, setTelp] = useState('');
  const [alamat, setAlamat] = useState('');
  const [img, setImg] = useState(
    'https://www.uii.ac.id/wp-content/uploads/2020/03/desain-rumah-sakit-uii-683x321.jpg',
  );
  const [latitude, setLatitude] = useState('22.6345648');
  const [longitude, setLongitude] = useState('88.4377279');
  const [number, setMateri] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});
  useEffect(() => {
    getData();
  }, []);
  const submit = () => {
    const data = {
      name,
      telp,
      alamat,
      img,
      latitude,
      longitude,
    };

    if (name && telp && alamat != '') {
      if (button === 'Simpan') {
        dispatch(postNomor(data));
        console.log('data dikirim');
        setName(''), setTelp(''), setAlamat('');
        getData();
      } else if (button === 'Update') {
        Axios.put(`${KOS_URL}/nomor/${selectedUser.id}`, data).then(() => {
          setButton('Simpan');
          setName(''), setTelp(''), setAlamat('');
          getData();
        });
      }
    } else {
      Alert.alert('Data tidak boleh kosong Min !');
    }
  };

  const getData = () => {
    Axios.get(`${KOS_URL}/nomor/`).then((res) => {
      console.log('data', res);
      setMateri(res.data);
    });
  };
  const selectItem = (item) => {
    console.log('selected item', item);
    setSelectedUser(item);
    setName(item.name);
    setAlamat(item.alamat);
    setTelp(item.telp);
    setLatitude(item.latitude);
    setLongitude(item.longitude);
    setButton('Update');
  };
  const deleteItem = (item) => {
    console.log(item);
    dispatch(deleteNomor(item));
    getData();
  };
  // const telpon = (value) => {
  //     const regex = /^(\+62|62)?[\s-]?0?8[1-9]{1}\d{1}[\s-]?\d{3,8}$/
  //     if(regex.test(telp) === true ){
  //         setTelp(value)
  //         console.log('mantap');
  //     }
  // }
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.textTitle}>Dashboard Admin</Text>
        <Text style={styles.textTitle}> Masukkan Data Materi </Text>
        <View style={{marginTop: 30}}>
          <TextInput
            placeholder="name"
            style={styles.input}
            value={name}
            onChangeText={(value) => setName(value)}
          />
          <TextInput
            placeholder="Alamat"
            style={styles.input}
            value={alamat}
            onChangeText={(value) => setAlamat(value)}
          />
          <TextInput
            placeholder="No Telepon"
            keyboardType="number-pad"
            style={styles.input}
            value={telp}
            onChangeText={(value) => setTelp(value)}
          />

          <TextInput
            placeholder="Lokasi"
            style={styles.input}
            value={longitude}
            onChangeText={(value) => setLongitude(value)}
          />
          <TextInput
            placeholder="Lokasi"
            style={styles.input}
            value={latitude}
            onChangeText={(value) => setLatitude(value)}
          />
          <TextInput
            placeholder="Image"
            style={styles.input}
            value={img}
            onChangeText={(value) => setImg(value)}
          />
        </View>
        <Button title={button} onPress={submit} />
        <View style={styles.line} />
        {number.map((number) => {
          return (
            <VItem
              key={number.id}
              name={number.name}
              alamat={number.alamat}
              telp={number.telp}
              onPress={() => selectItem(number)}
              onDelete={() =>
                Alert.alert(
                  'Peringatan',
                  'anda yakin akan menghapus user ini ?',
                  [
                    {
                      text: 'Tidak',
                      onPress: () => console.log('button tidak'),
                    },
                    {
                      text: 'YA',
                      onPress: () => deleteItem(number),
                    },
                  ],
                )
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default ANumberComponent;
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
  line: {
    height: 2,
    backgroundColor: 'grey',
    marginVertical: 20,
  },
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 20,
  },
  itemContainer: {flexDirection: 'row', marginBottom: 8},
  alamat: {marginLeft: 18, flex: 2},
  descName: {fontSize: 18, fontWeight: 'bold', color: 'red'},
  descEmail: {fontSize: 16},
  descBidang: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 20, color: 'red'},
});
