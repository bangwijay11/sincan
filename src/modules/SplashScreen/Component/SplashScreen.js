import {Image, StatusBar, StyleSheet, View} from 'react-native';

import React from 'react';

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <Image source={require('../../../property/images/sincan.png')} />
      
    </View>
  );
};
export default SplashScreen;

const styles = StyleSheet.create ({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:  1,
  }
})
