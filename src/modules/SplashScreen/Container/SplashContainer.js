import React, { useEffect } from 'react'
import { Text, View } from 'react-native'

import { Alert } from 'react-native';
import SplashScreen from '../Component/SplashScreen'

const SplashContainer = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
          navigation.replace('OnBoarding');
        }, 2000);
      });
    return (
        <SplashScreen/>
    )
}

export default SplashContainer
