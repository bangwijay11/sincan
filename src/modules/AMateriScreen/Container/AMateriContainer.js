import React, { useState } from 'react';
import { deleteMateri, setMateriInput } from '../Stores/Actions/AMateriAction';
import { useDispatch, useSelector } from 'react-redux';

import AMateriComponent from '../Component/AMateriComponent'
import { Alert } from 'react-native';
import Axios from 'axios';

const AMateriContainer = (props) => {
    const dispatch = useDispatch();
    const [materi, setMateri] = useState([])
    const {dataMateri} = useSelector((state) => state.AMateriReducer)

    const onInputChange = (value, inputType) => {
        dispatch(setMateriInpu(inputType, value));
        console.log('name', value);
    };

    return (
        <AMateriComponent
            // valueBab={{ dataMateri }.dataMateri.bab}
            // changeBab={(value) => onInputChange(value, 'bab')}

            // valueDesc={{ dataMateri }.dataMateri.desc}
            // changeDesc={(value) => onInputChange(value, 'desc')}

            // valueisi={{ dataMateri }.dataMateri.isi}
            // changeIsi={(value) => onInputChange(value, 'isi')}
            {...props} />
    )
}

export default AMateriContainer