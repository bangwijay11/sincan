import { ADD_MATERI, ADD_MATERI_ERROR, ADD_MATERI_SUCCESS, FETCH_MATERI, FETCH_MATERI_ERROR, FETCH_MATERI_SUCCESS, SET_MATERI } from "../../../../app/store/redux/action";

const initialState = {
    dataMateri: {
        bab: '',
        isi: '',
        desc: '',
    },
    data: [],
    error: null,
    loading: true,
    source: null
};

const AMateriReducer = (state = initialState, action) => {
    const newsState = Object.assign({}, state)
    console.log(action);
    switch (action.type) {
        case SET_MATERI:
            return {
                ...state,
                dataMateri: {
                    ...state.dataMateri,
                    [action.inputType]: action.inputValue,
                },
            };
            case FETCH_MATERI:
                return{
                    ...state,
                    loading: true,
                    error: null
                }
            case FETCH_MATERI_SUCCESS:
                return{
                    ...state,
                    loading: false,
                    error: null,
                    data: action.payload
                }
            case FETCH_MATERI_ERROR:
                return{
                    ...state,
                    loading: false,
                    error: action.error,
                    data: []
                }
        
        default:
            return state;
    }
};

export default AMateriReducer