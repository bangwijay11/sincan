import {
  ADD_MATERI_SUCCESS,
  DELETE_MATERI_ERROR,
  DELETE_MATERI_SUCCESS,
  SET_MATERI,
  UPDATE_MATERI_ERROR,
  UPDATE_MATERI_SUCCESS,
} from '../../../../app/store/redux/action';

import Axios from 'axios';
import {KOS_URL} from '@env';
import fetch from 'cross-fetch';

export const addMateriSuccess = (payload) => ({
  type: ADD_MATERI_SUCCESS,
  payload,
});

export const addMateriError = (error) => ({
  type: ADD_MATERI_ERROR,
  error,
});

export const deleteMateriSuccess = (payload) => ({
  type: DELETE_MATERI_SUCCESS,
  payload,
});

export const deleteMateriError = (error) => ({
  type: DELETE_MATERI_ERROR,
  error,
});

export const updateMateriSuccess = (payload) => ({
  type: UPDATE_MATERI_SUCCESS,
  payload,
});

export const updateMateriError = (error) => ({
  type: UPDATE_MATERI_ERROR,
  error,
});

export const setMateriInput = (inputType, value) => {
  return {type: SET_MATERI, inputType: inputType, inputValue: value};
};

export const fetchMateri = () => ({
  type: 'FETCH_MATERI',
});
export const fetchMateriSuccess = (payload) => ({
  type: FETCH_MATERI_SUCCESS,
  payload,
});
export const fetchMateriError = (error) => ({
  type: FETCH_MATERI_ERROR,
  error,
});

export const postMateri = (data) => {
  return (dispatch) => {
    Axios.post(`${KOS_URL}/materi`, data)
      .then((res) => {
        dispatch({
          type: 'ADD_MATERI',
          payload: data,
        });
        if (!data.error) {
          dispatch(addMateriSuccess(data));
        } else {
          dispatch(addMateriError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getMateri = () => {
  return (dispatch) => {
    Axios.get(`${KOS_URL}/materi`)
      .then((res) => {
        dispatch(fetchMateri());
        if (!data.error) {
          dispatch(fetchMateriSuccess(data));
        } else {
          dispatch(fetchMateriError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const deleteMateri = (item) => {
  return (dispatch) => {
    Axios.delete(`${KOS_URL}/materi/${item.id}`)
      .then((res) => {
        dispatch({
          type: 'DELETE_MATERI',
          payload: data,
        });

        if (!data.error) {
          dispatch(deleteMateriSuccess(data));
        } else {
          dispatch(deleteMateriError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
