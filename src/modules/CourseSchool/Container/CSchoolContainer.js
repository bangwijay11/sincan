import { Text, View } from 'react-native'

import CSchoolComponent from '../Component/CSchoolComponent'
import React from 'react'

const CSchoolContainer = (props) => {
    return (
        <CSchoolComponent {...props}/>
    )
}

export default CSchoolContainer
