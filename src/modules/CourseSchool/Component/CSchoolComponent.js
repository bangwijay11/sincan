import { BackButton, CardSchool } from '../../../components'

import { Image } from 'react-native'
import { ImageBackground } from 'react-native'
import React from 'react'
import { ScrollView } from 'react-native'
import { StatusBar } from 'react-native'
import { StyleSheet } from 'react-native'
import { View } from 'react-native'
import images from '../../../app/config/images'

const CSchoolComponent = (props) => {
  const {judul, isi} = props.route.params
    return (
        <ImageBackground
        source={images.bg_subcourse}
        style={styles.container}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <BackButton/>
        <View style={styles.imgContainer}>
          <Image
            source={images.logo}
            style={styles.imgLogo}
          />
        </View>
        <ScrollView>
            <View style={styles.contentContainer} >
                <CardSchool bab={judul} desc={isi}  
                // move={() => props.navigation.navigate('Done')}
                />
            </View>
        </ScrollView>
        
         
       
      </ImageBackground>
    )
}

export default CSchoolComponent
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    back: {
      marginTop: 24,
      marginLeft: 20,
    },
    iconBack: {marginTop: 20, marginLeft: 20},
    imgContainer: {width: 185, height: 63, alignSelf: 'center'},
    contentContainer: {alignItems: 'center', marginTop: 50, paddingHorizontal:35, marginBottom: 40 },
    imgLogo: {width: '100%', height: '100%'}
  });
  