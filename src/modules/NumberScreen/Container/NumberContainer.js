import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';

import Axios from 'axios';
import { Linking } from 'react-native';
import NumberComponent from '../Component/NumberComponent';
import { Platform } from 'react-native';
import { useSelector } from 'react-redux';

const NumberContainer = (props) => {
  const [datas, seData] = useState([]);
  const NumberReducer = useSelector((state) => state.NumberReducer);
  console.log('Number', NumberReducer);
  const getdata = () => {
    Axios.get('http://192.168.0.104:3000/nomor/')
      .then((res) => {
        seData(res.data);
        console.log('res data Materi : ', res.data);
      })
      .catch((err) => console.log('err0r : ', err));
  };
  //   useEffect(() => {
  //     getdata();
  //   }, []);

  const openDial = (number) => {
    if (Platform.OS === 'android') {
      Linking.openURL(`tel: ${number}`);
    } else {
      Linking.openURL('telprompt:085747144679');
    }
  };
  return <NumberComponent call={openDial} {...props} />;
};

export default NumberContainer;
