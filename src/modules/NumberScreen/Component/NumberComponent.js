import MapView, { Callout } from 'react-native-maps';
import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { BarStatus } from '../../../components';
import { Colors } from '../../../property/colors/colors';
import { Image } from 'react-native';
import { ScrollView } from 'react-native';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { getNumber } from '../Stores/Actions/NumberAction';

const NumberComponent = (props) => {
  const NumberReducer = require('../Component/number.json');
  const dispatch = useDispatch();

  //   useEffect(() => {
  //     dispatch(getNumber());
  //   }, []);

  return (
    <View>
      <ScrollView style={styles.ScrollView}>
        <BarStatus />
        {NumberReducer.map((NumberReducer) => (
          <View style={styles.contaner} key={NumberReducer.id}>
            <View style={styles.card} key={NumberReducer.id}>
              <View style={styles.imgContainer}>
                <Image
                  source={{ uri: `${NumberReducer.img}` }}
                  style={styles.img}
                />
              </View>
              <View style={styles.infoContainer}>
                <View style={styles.nameContainer}>
                  <Text style={styles.name}>{NumberReducer.name}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => props.call(NumberReducer.telp)}
                  style={styles.telpContainer}
                >
                  <Text style={styles.textTelp}>{NumberReducer.telp}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.mapContainer}>
                <MapView
                  style={{ flex: 1, width: window.width }} //window pake Dimensions
                  region={{
                    latitude: 22.62938671242907,
                    longitude: 88.4354486029795,
                    latitudeDelta: 0.04864195044303443,
                    longitudeDelta: 0.040142817690068,
                  }}
                >
                  <MapView.Marker
                    coordinate={{
                      latitude: 22.6293867,
                      longitude: 88.4354486,
                    }}
                  >
                    <Callout tooltip={false}>
                      <View style={styles.tooltip}>
                        <Text>{NumberReducer.name}</Text>
                      </View>
                    </Callout>
                  </MapView.Marker>
                </MapView>
              </View>
              <View style={styles.alamatContainer}>
                <Text style={styles.alamat}>{NumberReducer.alamat}</Text>
              </View>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

export default NumberComponent;
const styles = StyleSheet.create({
  contaner: { marginTop: 60, alignItems: 'center' },
  ScrollView: {
    marginBottom: 20,
    backgroundColor: Colors.background,
  },
  imgContainer: {
    width: 261,
    height: 138,
    backgroundColor: 'grey',
    marginTop: 15,
    borderRadius: 15,
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 15,
  },
  nameContainer: {
    marginRight: 8,
    height: 40,
    backgroundColor: 'white',
    borderRadius: 15,
    justifyContent: 'center',
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
  img: {
    height: '100%',
    width: '100%',
  },
  tooltip: {
    width: 100,
    height: 100,
  },

  name: { fontSize: 12, paddingHorizontal: 8, textAlign: 'center' },
  telpContainer: {
    width: 120,
    height: 40,
    backgroundColor: 'white',
    borderRadius: 15,
    justifyContent: 'center',
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
  textTelp: { fontSize: 12, textAlign: 'center' },
  mapContainer: { width: 228, height: 150, marginBottom: 20 },
  alamatContainer: {
    height: 50,
    backgroundColor: 'white',
    marginBottom: 20,
    borderRadius: 15,
    justifyContent: 'center',
    paddingHorizontal: 8,
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 7,
  },
  alamat: {
    textAlign: 'center',
  },
  card: {
    width: 320,
    backgroundColor: 'white',
    borderRadius: 15,
    alignItems: 'center',
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
});
