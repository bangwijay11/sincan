import {
  FETCH_NUMBER,
  FETCH_NUMBER_ERROR,
  FETCH_NUMBER_SUCCESS,
} from '../../../../app/store/redux/action';

import { KOS_URL } from '@env';
import fetch from 'cross-fetch';

export const fetchNumber = () => ({
  type: FETCH_NUMBER,
});
export const fetchNumberSuccess = (payload) => ({
  type: FETCH_NUMBER_SUCCESS,
  payload,
});
export const fetchNumberError = (error) => ({
  type: FETCH_NUMBER_ERROR,
  error,
});

export const callApi = async (URL) => {
  try {
    const res = await fetch(URL);

    if (res.status >= 400) {
      throw new Error('Bad response from server');
    }

    const data = await res.json();

    console.log(data);
    return data;
  } catch (err) {
    return { error: err.message };
  }
};

export const getNumber = () => (dispatch) => {
  dispatch(fetchNumber());
  callApi(`${KOS_URL}/nomor`).then((data) => {
    if (!data.error) {
      dispatch(fetchNumberSuccess(data));
    } else {
      dispatch(fetchNumberError(data.error));
    }
  });
};
