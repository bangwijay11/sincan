import { FETCH_NUMBER, FETCH_NUMBER_ERROR, FETCH_NUMBER_SUCCESS } from '../../../../app/store/redux/action';

const initialState = {
    loading: false,
    source: null,
    data: [],
    error: null,
}

const NumberReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_NUMBER:
            return{
                ...state,
                loading: true,
                error: null
            }
        case FETCH_NUMBER_SUCCESS:
            return{
                ...state,
                loading: false,
                error: null,
                data: action.payload
            }
        case FETCH_NUMBER_ERROR:
            return{
                ...state,
                loading: false,
                error: action.error,
                data: []
            }
    
        default:
            return state;
    }
}

export default NumberReducer;