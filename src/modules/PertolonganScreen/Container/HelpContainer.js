import { Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import HelpScreen from '../Component/HelpScreen'
import React from 'react'

const HelpContainer = (props) => {
    const PertolonganReducer = useSelector(state => state.PertolonganReducer)
    const dispatch = useDispatch();
  
    
    return (
        <HelpScreen {...props}/>
    )
}

export default HelpContainer
