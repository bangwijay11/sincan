import {
  FETCH_MATERI,
  FETCH_MATERI_ERROR,
  FETCH_MATERI_SUCCESS,
} from '../../../../app/store/redux/action';

import { KOS_URL } from '@env';
import fetch from 'cross-fetch';

export const fetchMateri = () => ({
  type: FETCH_MATERI,
});
export const fetchMateriSuccess = (payload) => ({
  type: FETCH_MATERI_SUCCESS,
  payload,
});
export const fetchMateriError = (error) => ({
  type: FETCH_MATERI_ERROR,
  error,
});

// function fetch api async/await
export const callApi = async (URL) => {
  try {
    const res = await fetch(URL);

    if (res.status >= 400) {
      throw new Error('Bad response from server');
    }

    const data = await res.json();

    console.log(data);
    return data;
  } catch (err) {
    // console.error(err);
    return { error: err.message };
  }
};

// funtion call api

export const getMateri = () => (dispatch) => {
  dispatch(fetchMateri());
  callApi(`${KOS_URL}/Pertolongan/`).then((data) => {
    console.log('DATA FROM API', data);
    if (!data.error) {
      dispatch(fetchMateriSuccess(data));
    } else {
      dispatch(fetchMateriError(data.error));
    }
  });
};
