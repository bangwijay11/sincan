import { BarStatus, CardChapter, CardHeaderClass } from '../../../components';
import { ImageBackground, StyleSheet } from 'react-native';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ActivityIndicator } from 'react-native';
import MATERI_STRING from '../../../app/store/string';
import { ScrollView } from 'react-native';
import { Text } from 'react-native';
import { View } from 'react-native';
import { getMateri } from '../Stores/Actions/PertolonganAction';
import images from '../../../app/config/images';

const HelpScreen = (props) => {
  // const PertolonganReducer = useSelector((state) => state.PertolonganReducer);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  // useEffect(() => {
  //   setTimeout(() => {
  //     // getMateri();
  //     setLoading(false);
  //   }, 2000);
  // }, []);
  const PertolonganReducer = require('../Component/Help.json');
  console.log('PertolonganReducer', PertolonganReducer);
  return (
    <ImageBackground source={images.bg_course} style={styles.container}>
      <BarStatus />
      <CardHeaderClass
        title={MATERI_STRING.PERTOLONGAN_PERTAMA}
        img={images.illustrator3}
      />
      {PertolonganReducer.map((PertolonganReducer) => (
        <ScrollView key={PertolonganReducer.id}>
          <CardChapter
            move={() =>
              props.navigation.navigate('CHelp', {
                isi: PertolonganReducer.isi,
                Judul: PertolonganReducer.Judul,
              })
            }
            title={PertolonganReducer.Judul}
            desc={PertolonganReducer.desc}
            Image={images.kotak_obat}
          />
        </ScrollView>
      ))}
    </ImageBackground>
  );
};

export default HelpScreen;

const styles = StyleSheet.create({
  ActivityIndicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    flex: 1,
  },
});
