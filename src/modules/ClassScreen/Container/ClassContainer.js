import React, { useEffect, useState } from 'react'

import ClassScreen from '../Component/CLassScreen'

const ClassContainer = (props) => {
    return (
      <ClassScreen 
      {...props} />
    )
}

export default ClassContainer
