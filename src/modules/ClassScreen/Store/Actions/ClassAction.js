import {
  FETCH_MATERI,
  FETCH_MATERI_ERROR,
  FETCH_MATERI_SUCCESS,
} from '../../../../app/store/redux/action';

import {Alert} from 'react-native';
import {KOS_URL} from '@env';
import fetch from 'cross-fetch';

console.log('kosurl', KOS_URL);
export const fetchMateri = () => ({
  type: FETCH_MATERI,
});
export const fetchMateriSuccess = (payload) => ({
  type: FETCH_MATERI_SUCCESS,
  payload,
});
export const fetchMateriError = (error) => ({
  type: FETCH_MATERI_ERROR,
  error,
});

// function fetch api async/await
export const callApi = async (URL) => {
  try {
    const res = await fetch(URL);

    if (res.status >= 400) {
      throw new Error('Bad response from server');
    }

    const data = await res.json();

    console.log(data);
    return data;
  } catch (err) {
    Alert.alert(
      'sepertinya ada yang salah',
      'silahkan cek koneksi internet kamu',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            getMateri;
          },
        },
      ],
      err.message,
    );
    // console.error('err', err.message);
    return {error: err.message};
  }
};

// funtion call api

export const getMateri = () => (dispatch) => {
  dispatch(fetchMateri());
  callApi(`${KOS_URL}/materi/`).then((data) => {
    // console.log('DATA FROM API', data);
    if (!data.error) {
      dispatch(fetchMateriSuccess(data));
    } else {
      dispatch(fetchMateriError(data.error));
    }
  });
};
