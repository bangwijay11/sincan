import { FETCH_MATERI, FETCH_MATERI_ERROR, FETCH_MATERI_SUCCESS } from '../../../../app/store/redux/action';

const initialState = {
    loading: false,
    source: null,
    data: [],
    error: null,
}

const ClassReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_MATERI:
            return{
                ...state,
                loading: true,
                error: null
            }
        case FETCH_MATERI_SUCCESS:
            return{
                ...state,
                loading: false,
                error: null,
                data: action.payload
            }
        case FETCH_MATERI_ERROR:
            return{
                ...state,
                loading: false,
                error: action.error,
                data: []
            }
    
        default:
            return state;
    }
}

export default ClassReducer;