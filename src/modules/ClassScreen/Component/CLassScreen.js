import { BackButton, CardChapter } from '../../../components/index';
import { Image, StatusBar, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';

import { ActivityIndicator } from 'react-native-paper';
import { Colors } from '../../../property/colors/colors';
import { ImageBackground } from 'react-native';
import MATERI_STRING from '../../../app/store/string';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { getMateri } from '../Store/Actions/ClassAction';
import images from '../../../app/config/images';

const ClassScreen = (props) => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      getMateri();
      setLoading(false);
    }, 2000);
  }, []);

  const { ClassReducer } = props;

  console.log('ClassReducer', ClassReducer);
  console.log('ClassReducer', loading);

  return ClassReducer.loading ? (
    <View style={styles.ActivityIndicator}>
      <ActivityIndicator animating={loading} color="#00f0ff" />
      <Text style={{ marginTop: 10 }} children="Mohon tunggu sebentar" />
    </View>
  ) : (
    <ImageBackground source={images.bg_course} style={styles.container}>
      <StatusBar backgroundColor="white" />
      <View style={styles.boxClass}>
        <BackButton />
        {/* <Text>{JSON.stringify(props)}</Text> */}
        <View style={styles.boxContainer}>
          <View style={styles.imgBox}>
            <Image source={images.illustrator1} style={styles.ilus} />
          </View>
          <View style={styles.textBox}>
            <Text style={styles.textBoxHeader}>
              {MATERI_STRING.PEMBELAJARAN}
            </Text>
          </View>
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      >
        {ClassReducer.data.map((ClassReducer) => (
          <View key={ClassReducer.id}>
            <CardChapter
              move={() =>
                props.navigation.navigate('Course', {
                  bab: ClassReducer.bab,
                  judul: ClassReducer.judul,
                  isi: ClassReducer.isi,
                })
              }
              title={ClassReducer.bab}
              desc={ClassReducer.desc}
              isi={ClassReducer.isi}
              Image={images.tsunami}
            />
          </View>
        ))}
      </ScrollView>
    </ImageBackground>
  );
};

const mapStateToProps = (state) => ({
  ClassReducer: state.ClassReducer,
});

const mapDispatchToProps = (dispatch) => ({
  getMateri: dispatch(getMateri()),
});
export default connect(mapStateToProps, mapDispatchToProps)(ClassScreen);

const styles = StyleSheet.create({
  ActivityIndicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    flex: 1,
  },
  boxClass: {
    width: '100%',
    height: 200,
    backgroundColor: '#2A89B4',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  backContainer: { marginTop: 0 },
  iconBack: {
    marginTop: 20,
    marginLeft: 20,
  },
  boxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ilus: {
    width: 107,
    height: 117,
  },
  textBox: {
    width: 150,
    height: 48,
  },
  textBoxHeader: { fontWeight: '700', fontSize: 20, color: 'white' },

  cardChapter: {
    width: 300,
    height: 110,
    backgroundColor: Colors.chapter,
    borderRadius: 15,
    marginTop: 15,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  textChapter: {
    color: '#FEFED5',
    fontSize: 15,
    fontWeight: '700',
  },
  textChapterDesc: {
    fontSize: 12,
    color: '#FEFED5',
    marginTop: 5,
  },
});
