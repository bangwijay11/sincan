import { Text, View } from 'react-native'

import NewsScreen from '../Component/NewsScreen'
import React from 'react'

const NewsContainer = (props) => {
    return (
        <NewsScreen {...props}/>
    )
}

export default NewsContainer
