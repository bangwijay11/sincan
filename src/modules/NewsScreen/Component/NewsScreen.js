import { BarStatus, NewsCard } from '../../../components';
import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';

import { Image } from 'react-native';
import { TouchableHighlight } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { getNews } from '../store/Action/NewsAction';

const NewsScreen = (props) => {
  const NewsReducer = useSelector(state => state.NewsReducer)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getNews())
  }, []);

  return (
    <View style={styles.container}>
      <BarStatus />
      <View style={styles.content}>
        <View style={styles.txtContainer}>
          <Text style={styles.txtHead}>Berita terbaru yang disarankan untukmu</Text>
        </View>
        <View>
          {/* <Image source={{ uri: `${user.photo}` }} style={styles.img} /> */}
        </View>
      </View>

      {NewsReducer.data.map((NewsReducer) => (
        <View style={styles.slide} key={NewsReducer.id}>
          <NewsCard
            judul={NewsReducer.judul}
            desc={NewsReducer.desc}
            photo={{ uri: NewsReducer.photo }}
            move={() => props.navigation.navigate('Detail', {
              isi: NewsReducer.isi,
              judul: NewsReducer.judul,
              image: { uri: NewsReducer.photo }
            })}
          />
        </View>

      ))
      }
    </View>

  )
}


export default NewsScreen
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    marginLeft: 20,
    marginRight: 20
  },
  slide: {
    justifyContent: 'center',
    backgroundColor: 'transparent',
    alignItems: 'center',
    flexDirection: 'row'
  },
  txtContainer: { width: 220 },
  txtHead: {
    fontWeight: '500',
    fontSize: 20
  },
  img: {
    width: 60,
    height: 60,
    borderRadius: 20
  }

})
