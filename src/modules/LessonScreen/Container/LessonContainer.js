import { Text, View } from 'react-native'

import LessonScreen from '../Component/LessonScreen'
import React from 'react'

const LessonContainer = (props) => {
    return (
      <LessonScreen {...props}/>
    )
}

export default LessonContainer
