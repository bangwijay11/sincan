import { Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { TEXT_DONE, TITLE_COURSE } from '../../../components/FontStyle/FontStyle'

import LinearGradient from 'react-native-linear-gradient'
import MATERI_STRING from '../../../app/store/string'
import React from 'react'
import images from '../../../app/config/images'

const LessonScreen = ({navigation}) => {
    return (
        <LinearGradient colors={['#60B6AD', '#51FFBA']} style={styles.container} >
        <StatusBar translucent backgroundColor="transparent" barStyle='dark-content' />
        <View style={styles.cntnContainer}>
            <Text style={TITLE_COURSE}>{MATERI_STRING.SELAMAT}</Text>
            <View style={styles.txtContainer}>
             <Text style={TEXT_DONE}>{MATERI_STRING.DESC_SELAMAT}</Text>
            </View>
            <View style={{marginTop: 25}}>
             <Image source={images.sincan} />
            </View>
            <View style={styles.done}>
             <Text style={TEXT_DONE}>{MATERI_STRING.DESC_LANJUT}</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.button}>
                <Text style={styles.text}>{MATERI_STRING.DONE}</Text>
            </TouchableOpacity>
        </View>       
        </LinearGradient>
    )
}

export default LessonScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    cntnContainer: {marginTop: 120, alignItems: 'center'},
    button: {
        width: 150, 
        height: 45, 
        backgroundColor: '#23ADAD', 
        borderRadius: 25, 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 75,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,
    },
    done:{
        width: 250, 
        alignItems: 'center', 
        paddingHorizontal: 20, 
        marginTop: 30
    },
    textContainer: {
        width: 246, alignItems: 'center', paddingHorizontal: 20
    },
    text: {
        color: 'white'
    }
})
