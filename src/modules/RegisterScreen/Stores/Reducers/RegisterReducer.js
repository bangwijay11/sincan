/* eslint-disable prettier/prettier */
const initialStateRegister = {
  form: {
    username: '',
    email: '',
    password: '',
  },
  
};

const RegisterReducer = (state = initialStateRegister, action) => {
  const newState = Object.assign({}, state)
  console.log(action);
  switch (action.type) {
    case 'SET_FORM':
      return {
        ...state,
        form: {
          ...state.form,
          [action.inputType]: action.inputValue,
        },
      };
    // case 'SUBMIT_USER': 
    // return {...newState}
  }
  return state;
};


export default RegisterReducer;