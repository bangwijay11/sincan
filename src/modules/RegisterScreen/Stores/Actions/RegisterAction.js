import {
  ADD_MATERI_ERROR,
  ADD_USER_SUCCESS,
} from '../../../../app/store/redux/action';

import Axios from 'axios';
import {KOS_URL} from '@env';

/* eslint-disable prettier/prettier */
export const setForm = (inputType, value) => {
  return {type: 'SET_FORM', inputType: inputType, inputValue: value};
};
export const addUser = (payload) => ({
  type: 'SUBMIT_USER',
  payload,
});
export const addUserSuccess = (payload) => ({
  type: ADD_USER_SUCCESS,
  payload,
});
export const addUserError = (error) => ({
  type: ADD_USER_ERROR,
  error,
});

export const submit = (data) => {
  return (dispatch) => {
    Axios.post(`${KOS_URL}/users`, data)
      .then((res) => {
        dispatch({
          type: 'SUBMIT_USER',
          payload: data,
        });
        if (!data.error) {
          dispatch(addUserSuccess(data));
        } else {
          dispatch(addUserError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
