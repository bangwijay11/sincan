import { Image, KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { TEXT_INPUT, TEXT_LOGIN } from '../../../components/FontStyle/FontStyle';

import { ButtonSign } from '../../../components';
import { Colors } from '../../../property/colors/colors'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';
import { StatusBar } from 'react-native';
import images from '../../../app/config/images';

const RegisterComponent = (props) => {
  const [enableShift, setenableShift] = useState(false)

  return (
    <View style={styles.container}>
      <StatusBar translucent barStyle="dark-content" backgroundColor="transparent" />
      <View style={styles.elipsContainer}>
        <LinearGradient
          colors={['#344A9D', '#60B6AD']}
          style={styles.elips} />
      </View>
      <KeyboardAvoidingView enabled={enableShift} behavior="position" style= {styles.FlexGrowOne}>
      <View style={styles.logoContainer}>
        <Image source={images.logo} style={styles.logo} />
      </View>
      <ScrollView>
      <SafeAreaView style={{flex:1}}>
      <View style={styles.formCOntainer}>
        <Text
          style={styles.text_label}>
          Nama Pengguna
                </Text>
        <View style={styles.action}>
          <FontAwesome name="user" color={Colors.primary} size={20} />
          <TextInput
            placeholder="Your Username"
            placeholderTextColor="#BDBDBD"
            style={[
              TEXT_INPUT, styles.textInput
            ]}
            autoCapitalize="none"
            value={props.valueName}
            onChangeText={props.nameChange}
            onFocus={() => setenableShift(true)}
            onEndEditing={props.nameEditing}
          />
          {props.animName}
        </View>
        {props.erorAnimName}
        <Text
          style={styles.text_label}>
          Email
                </Text>
        <View style={styles.action}>
          <FontAwesome name="envelope" color={Colors.primary} size={20} />
          <TextInput
            placeholder="Your Email"
            placeholderTextColor="#BDBDBD"
            style={[
              TEXT_INPUT, styles.textInput
            ]}
            autoCapitalize="none"
            keyboardType='email-address'
            onFocus={() => setenableShift(true)}
            onChangeText={props.emailChange}
            value={props.valueEmail}
          />
          {props.animEmail}
        </View>
        {props.erorEmail}
        <Text
          style={[
            styles.text_label,
            {
              marginTop: 25,
            },
          ]}>
          Password
                </Text>
        <View style={styles.action}>
          <FontAwesome name="lock" color={Colors.primary} size={30} />
          <TextInput
            placeholder="Your Password"
            placeholderTextColor="#BDBDBD"
            secureTextEntry={props.secureTextEntry}
            style={[
              TEXT_INPUT, styles.textInput
            ]}
            autoCapitalize="none"
            onChangeText={props.passChange}
            onFocus={() => setenableShift(true)}
            value={props.valuePass}
          />
          <TouchableOpacity onPress={props.security}>
            {props.showPass}
          </TouchableOpacity>
        </View>
        {props.erorPass}
      </View>
      <View style={styles.btnLoginContainer}>
        <ButtonSign 
          onPress={props.register}
          disabled={props.disabled}
          title="Daftar"
        />
      </View>
      <View style={styles.btnRegisterContainer}>
        <View>
          <Text style={styles.textAkun}>Sudah Punya Akun?</Text>
        </View>
        <TouchableOpacity onPress={props.login} style={styles.btnReg}>
          <Text style={styles.textRegister}>Masuk</Text>
        </TouchableOpacity>
      </View>
      </SafeAreaView>
      </ScrollView>
      </KeyboardAvoidingView>
    </View>
  )
}

export default RegisterComponent

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.background,
    flex: 1,
  },
  elipsContainer: { alignItems: 'flex-end' },
  elips: { width: 110, height: 110, borderBottomLeftRadius: 200 },
  logoContainer: { alignItems: 'center' },
  logo: { width: 200, height: 70 },
  formCOntainer: {
    marginTop: 60,
    paddingHorizontal: 20
  },

  text_label: {
    color: '#BDBDBD',
    fontSize: 14,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#BDBDBD',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  btnLogin: {
    width: 314,
    height: 42,
    borderRadius: 15,
    backgroundColor: Colors.primary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnLoginContainer: {
    alignItems: 'center',
    marginTop: 30
  },
  btnRegisterContainer: {
    marginTop: 8,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  textAkun: { color: '#8C8C8C' },
  btnReg: { marginLeft: 8 },
  textRegister: { color: Colors.primary }

})
