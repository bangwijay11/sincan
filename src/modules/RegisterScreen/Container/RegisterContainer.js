import * as Animatable from 'react-native-animatable';

import React, { useEffect, useState } from 'react';
import { setForm, submit } from '../Stores/Actions/RegisterAction';
import { useDispatch, useSelector } from 'react-redux';
import firestore from '@react-native-firebase/firestore';
import { Alert } from 'react-native';
import Axios from 'axios';
import { Colors } from '../../../property/colors/colors';
import Feather from 'react-native-vector-icons/Feather';
// import { KOS_URL } from '@env';
import RegisterComponent from '../Component/RegisterComponent';
import { StyleSheet, ToastAndroid } from 'react-native';
import { Text } from 'react-native';
import auth from '@react-native-firebase/auth';

const RegisterContainer = (props) => {
  const { form } = useSelector((state) => state.RegisterReducer);
  const dataUser = { form }.form;
  const dispatch = useDispatch();
  const [data, setData] = React.useState({
    check_textInputChange: false,
    check_emailChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidEmail: true,
    isValidPassword: true,
    disableButton: true,
  });

  const textInputChange = () => {
    if ({ form }.form.username.trim().length !== 0) {
      setData({
        ...data,
        check_textInputChange: true,
        isValidUser: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        check_textInputChange: false,
        isValidUser: false,
        disableButton: true,
      });
    }
  };
  const handleEmailChange = () => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (
      { form }.form.email.trim().length !== 0 &&
      regex.test({ form }.form.email) === true
    ) {
      setData({
        ...data,
        isValidEmail: true,
        check_emailChange: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        isValidEmail: false,
        check_emailChange: false,
        disableButton: true,
      });
    }
  };

  const onInputChange = (value, inputType) => {
    if (inputType === 'email') {
      handleEmailChange();
    } else if (inputType === 'username') {
      textInputChange();
    } else {
      handlePasswordChange();
    }
    dispatch(setForm(inputType, value));
    console.log('name', value);
  };

  const handleValidUser = () => {
    if ({ form }.form.username.trim().length !== 0) {
      setData({
        ...data,
        isValidUser: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
        disableButton: true,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };
  const handlePasswordChange = () => {
    if ({ form }.form.password.trim().length >= 7) {
      setData({
        ...data,
        isValidPassword: true,
        disableButton: false,
      });
    } else {
      setData({
        ...data,
        isValidPassword: false,
        disableButton: true,
      });
    }
  };

  const login = () => {
    props.navigation.navigate('Login');
    (form.username = ''), (form.email = ''), (form.password = '');
    setData({
      data,
    });
  };

  const handleRegister = () => {
    if (dataUser.username && dataUser.password && dataUser.email !== '') {
      auth()
        .createUserWithEmailAndPassword(dataUser.email, dataUser.password)
        .then(() => {
          // Route.navigate(Route.Dashboard);
          firestore()
            .collection('users')
            .add({
              name: dataUser.username,
              email: dataUser.email,
              dob: '2 Mei 2002',
              gender: 'Male',
            })
            .then(() => {
              console.log('User added!');
            });
          props.navigation.navigate('Login');
        })
        .catch((error) => {
          ToastAndroid.show(error.code, ToastAndroid.SHORT);
        });
    }
  };

  return (
    <>
      <RegisterComponent
        data
        valueName={{ form }.form.username}
        nameChange={(value) => onInputChange(value, 'username')}
        nameEditing={(e) => handleValidUser(e.nativeEvent.text)}
        animName={
          data.check_textInputChange ? (
            <Animatable.View animation="bounceIn">
              <Feather name="check-circle" color="green" size={20} />
            </Animatable.View>
          ) : null
        }
        erorAnimName={
          data.isValidUser ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>username tidak boleh kosong</Text>
            </Animatable.View>
          )
        }
        valueEmail={{ form }.form.email}
        emailChange={(value) => onInputChange(value, 'email')}
        animEmail={
          data.check_emailChange ? (
            <Animatable.View animation="bounceIn">
              <Feather name="check-circle" color="green" size={20} />
            </Animatable.View>
          ) : null
        }
        erorEmail={
          data.isValidEmail ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>Email tidak sesuai</Text>
            </Animatable.View>
          )
        }
        valuePass={{ form }.form.password}
        passChange={(value) => onInputChange(value, 'password')}
        secureTextEntry={data.secureTextEntry ? true : false}
        security={updateSecureTextEntry}
        showPass={
          data.secureTextEntry ? (
            <Feather name="eye-off" color="grey" size={20} />
          ) : (
            <Feather name="eye" color="grey" size={20} />
          )
        }
        erorPass={
          data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>
                Password must be 8 characters long.
              </Text>
            </Animatable.View>
          )
        }
        login={login}
        register={handleRegister}
        disabled={data.disableButton}
        {...props}
      />
    </>
  );
};

export default RegisterContainer;
const styles = StyleSheet.create({
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  btnLogin: {
    width: 314,
    height: 42,
    borderRadius: 15,
    backgroundColor: Colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
