// headerImage={require("../../../property/images/news.png")}

import * as Animatable from 'react-native-animatable';

import {
  Dimensions,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import HeaderImageScrollView, {
  TriggeringView,
} from 'react-native-image-header-scroll-view';
import React, { useRef } from 'react';

import { BackIcon } from '../../../property';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MapView from 'react-native-maps';

const MIN_HEIGHT = Platform.OS === 'ios' ? 90 : 60;
const MAX_HEIGHT = 300;

const DetailNewsScreen = (props) => {
  const navTitleView = useRef(null);
  const {isi, judul, image} = props.route.params
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <HeaderImageScrollView showsVerticalScrollIndicator={false}
        maxHeight={MAX_HEIGHT}
        minHeight={MIN_HEIGHT}
        maxOverlayOpacity={0.6}
        minOverlayOpacity={0.3}
        renderHeader={() => (
          <Image source={image} style={styles.image} />
        )}
        renderForeground={() => (
          <View style={styles.titleContainer}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
              <BackIcon style={styles.navTitle} width={24} height={24} />
            </TouchableOpacity>
          </View>
        )}
        renderFixedForeground={() => (
          <Animatable.View style={styles.navTitleView} ref={navTitleView}>
            <Text style={styles.title}>{judul}</Text>

          </Animatable.View>
        )}>
        <TriggeringView
          style={styles.section}
          onHide={() => navTitleView.current.fadeInUp(200)}
          onDisplay={() => navTitleView.current.fadeOut(200)}>
          <View style={styles.shareContainer}>
            <Text style={styles.titleFix}>{judul}</Text>
            <TouchableOpacity onPress={props.move} style={styles.share}>
              <FontAwesome name="share-alt" size={20} color="#FF6347" />
              <Text style={styles.txtShare}>Share</Text>
            </TouchableOpacity>
          </View>
        </TriggeringView>
        <View style={[styles.section, styles.sectionLarge]}>
          <Text style={styles.sectionContent}>{isi}</Text>
        </View>

        <View style={[styles.section, { height: 250 }]}>
          <MapView
            style={{ flex: 1, width: window.width }} 
            region={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }} >
            <MapView.Marker
              coordinate={{
                latitude: 37.78825,
                longitude: -122.4324,
              }}
              title="Lokasi"
              description="Bencana" />
          </MapView>
        </View>
      </HeaderImageScrollView>
    </View>
  );
};

export default DetailNewsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: MAX_HEIGHT,
    width: Dimensions.get('window').width,
    alignSelf: 'stretch',
    resizeMode: 'cover',
  },
  title: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',

  },
  name: {
    fontWeight: 'bold',
  },
  section: {
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    backgroundColor: 'white',


  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  sectionContent: {
    fontSize: 16,
    textAlign: 'justify',
  },
  categories: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
  },
  categoryContainer: {
    flexDirection: 'row',
    backgroundColor: '#FF6347',
    borderRadius: 20,
    margin: 10,
    padding: 10,
    paddingHorizontal: 15,
  },
  category: {
    fontSize: 14,
    color: '#fff',
    marginLeft: 10,
  },
  titleContainer: {
    flex: 1,
    // alignSelf: 'stretch',
    // justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 30
  },
  imageTitle: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: 24,
  },
  navTitleView: {
    height: MIN_HEIGHT,
    justifyContent: 'center',
    // justifyContent:'space-evenly',
    alignItems: 'center',
    paddingTop: Platform.OS === 'ios' ? 40 : 15,
    opacity: 0,
    flexDirection: 'row'
  },
  navIconView: {
    height: MIN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: Platform.OS === 'ios' ? 40 : 5,
    opacity: 0,
  },
  navTitle: {
    color: 'white',
    fontSize: 18,
    backgroundColor: 'transparent',
    marginLeft: 20
  },
  sectionLarge: {
    minHeight: 300,
  },
  shareContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-between' 
  },
  share: { 
    flexDirection: 'row', 
    alignItems: 'flex-end'
 },
  txtShare: { 
    marginHorizontal: 2, 
    marginLeft: 10 
  }
});