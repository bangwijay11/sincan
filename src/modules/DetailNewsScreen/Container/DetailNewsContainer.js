import DetailNewsScreen from '../Component/DetailNewsScreen'
import React from 'react'
import Share from 'react-native-share'

const DetailNewsContainer = (props) => {
    const myCustomShare = async() => {
        const shareOptions = {
          message: 'Order your next meal from FoodFinder App. I\'ve already ordered more than 10 meals on it.',
        }
    
        try {
          const ShareResponse = await Share.open(shareOptions);
          console.log(JSON.stringify(ShareResponse));
        } catch(error) {
          console.log('Error => ', error);
        }
      };
    return (
        <DetailNewsScreen 
        move={myCustomShare}
        {...props}/>
    )
}

export default DetailNewsContainer
