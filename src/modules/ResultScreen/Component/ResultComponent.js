import { Image, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'

import { Alert } from 'react-native'
import { BarStatus } from '../../../components'
import { Button } from 'react-native'
import { Colors } from '../../../property/colors/colors'
import LinearGradient from 'react-native-linear-gradient'
import Modal from 'react-native-modal';
import Pie from 'react-native-pie'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native'
import images from '../../../app/config/images'

const ResultComponent = (props) => {

    const { result, correct, incorrect } = props.route.params;

    return (
        <LinearGradient colors={['#6FC9C0', '#F1F4F7']} style={{ flex: 1 }}>
            <BarStatus />
            <View>
                <View style={styles.greet}>
                    <Text style={styles.txtGreet}>Selamat!</Text>
                </View>
                <View style={styles.arcContainer}>
                    <View style={{ width: 160, height: 160 }}>
                        <Pie
                            radius={80}
                            innerRadius={75}
                            sections={[
                                {
                                    percentage: result,
                                    color: Colors.primary,
                                },
                            ]}
                            backgroundColor="#ddd"
                        />
                        <View style={styles.gauge}>
                        {result < 50 ?  <Image 
                            source={images.danger}
                            style={styles.gaugeText}>
                                
                            </Image> : 
                            <Image 
                            source={images.sincan}
                            style={styles.gaugeText}>
                                
                            </Image> 
                             }
                        
                            
                        </View>

                    </View>
                    <View style={styles.nilai}>
                        <Text style={styles.txtNilai}>Nilai Kamu {result}</Text>
                        <Text style={styles.txtNilai}>Ayoo Coba & Belajar lagi</Text>
                        <Text style={styles.txtNilai}>Kamu benar {correct}</Text>
                        <Text style={styles.txtNilai}>Kamu salah {incorrect}</Text>
                    </View>
                    <View style={styles.BtnContainer}>
                        <TouchableOpacity style={styles.btn} onPress={() => props.navigation.navigate('Home')}>
                            <Text style={styles.txtBtn}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btn} onPress={() => props.navigation.navigate('Class')}>
                            <Text style={styles.txtBtn}>Belajar</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        </LinearGradient>

    )
}

export default ResultComponent
const styles = StyleSheet.create({
    greet: { alignItems: 'center', marginTop: 47 },
    txtGreet: { color: 'white', fontWeight: 'bold', fontSize: 32 },
    arcContainer: { marginTop: 33, alignItems: 'center' },
    nilai: { marginTop: 34, alignItems: 'center' },
    txtNilai: { color: 'white', fontWeight: '500', fontSize: 20 },
    txtBtn: { color: 'white', fontSize: 14 },
    btn: {
        width: 150, height: 42, padding: 5, backgroundColor: '#6FC9C0', borderRadius: 25, alignItems: 'center', margin: 20,
        justifyContent: 'center',
        shadowColor: "#6FC9C0",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 1.41,
        shadowRadius: 9.11,

        elevation: 10,
    },
    BtnContainer: {
        marginTop: 60,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    gauge: {
        position: 'absolute',
        width: 160, height: 160,
        alignItems: 'center',
        justifyContent: 'center',
    },
    gaugeText: {
        backgroundColor: 'transparent',
        width: 90,
        height: 90,
        resizeMode: 'contain'
    },

})
