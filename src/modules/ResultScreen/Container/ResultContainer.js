import { Text, View } from 'react-native'

import React from 'react'
import ResultComponent from '../Component/ResultComponent'

const ResultContainer = (props) => {
    return (
        <ResultComponent {...props}/>
    )
}

export default ResultContainer
