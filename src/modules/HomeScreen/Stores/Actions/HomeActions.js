export const setProfileName = (username) => {
  return {
    type: 'SET_USERNAME',
    payload: username,
  };
};
