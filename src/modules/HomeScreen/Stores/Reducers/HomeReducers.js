import { combineReducers } from 'redux';

const initialState = {
  username: 'Rony Arya',
};

const ListProdukReducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);
  console.log(action);
  switch (action.type) {
    case 'SET_USERNAME':
      return {
        ...state,
        username: action.payload,
      };
    case 'AMBIL_DATA':
      return action.data;
    default:
      return state;
  }
};

export default combineReducers({
  lisProduk: ListProdukReducer,
});
