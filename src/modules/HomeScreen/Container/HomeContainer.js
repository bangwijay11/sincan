import React, { useEffect, useState } from 'react';

import { Alert } from 'react-native';
import Axios from 'axios';
import HomeScreen from '../Component/HomeScreen';
import { ToastAndroid } from 'react-native';
import { useSelector } from 'react-redux';
import firestore from '@react-native-firebase/firestore';

const HomeContainer = (props) => {
  const { username } = useSelector(
    (state) => state.ListProdukReducer.lisProduk,
  );

  const detect = () => {
    const today = new Date();
    const hour = today.getHours();
    if (hour < 12) {
      return 'Selamat Pagi';
    } else if (hour < 18) {
      return 'selamat siang';
    }
    return 'Selamat Malam';
  };

  return (
    <HomeScreen
      hai={detect()}
      name={username}
      call={() => props.navigation.navigate('Number')}
      {...props}
    />
  );
};

export default HomeContainer;
