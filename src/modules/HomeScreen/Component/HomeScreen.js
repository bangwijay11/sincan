import { Image, StatusBar, StyleSheet, Text } from 'react-native';

import { Alert, View } from 'react-native';
import { CardMenu } from '../../../components';
import { Colors } from '../../../property/colors/colors';
import MATERI_STRING from '../../../app/store/string';
import React from 'react';
import { ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native';
import images from '../../../app/config/images';

const HomeScreen = (props) => {
  const createTwoButtonAlert = () =>
    Alert.alert(
      'Apakah kamu yakin memulai latihan soal?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => props.navigation.navigate('Quiz') },
      ],
      { cancelable: false },
    );
  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <View>
        <Image source={images.home_ellips} style={styles.topImage} />
        <View style={styles.containerBottomImage}>
          <Image source={images.home_bottom} style={styles.bottomImage} />
        </View>

        <View style={styles.drawerContainer}>
          <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
            <Image style={styles.drawerIcon} source={images.drawe} />
          </TouchableOpacity>

          <View style={styles.dangerContainer}>
            <TouchableOpacity onPress={props.call} style={styles.dangerCircle}>
              <Image style={styles.dangerIcon} source={images.danger} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.headerContainer}>
          <Text style={styles.textHeader}>
            Hai! {props.hai} {props.name} Silahkan Pilih Pembelajaranmu
          </Text>
        </View>
      </View>
      <View style={styles.cardBox}>
        <ScrollView>
          <View style={styles.cardContainer} {...props}>
            <CardMenu
              move={() => props.navigation.navigate('Class')}
              color="#6FCF97"
              marginTop={136}
              title={MATERI_STRING.BELAJAR}
              Image={images.illustrator1}
            />
            <CardMenu
              move={createTwoButtonAlert}
              color="#56CCF2"
              marginTop={156}
              title={MATERI_STRING.SOAL}
              Image={images.quiz}
            />
            <CardMenu
              move={() => props.navigation.navigate('Pertolongan')}
              color="#56CCF2"
              marginTop={10}
              title={MATERI_STRING.PERTOLONGAN}
              Image={images.illustrator3}
            />
            <CardMenu
              move={() => props.navigation.navigate('Sekolah')}
              color="#6FCF97"
              marginTop={20}
              title={MATERI_STRING.SEKOLAH}
              Image={images.illustrator4}
            />
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  bottomImage: {
    width: '100%',
    height: 140,
    marginTop: 60,
  },
  cardBox: { position: 'absolute', paddingHorizontal: 24, marginTop: 60 },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    width: '100%',
  },
  container: {
    backgroundColor: Colors.beckground,
    flex: 1,
  },
  containerBottomImage: {
    position: 'relative',
    marginTop: 360,
    flex: 1,
  },
  dangerContainer: {
    alignItems: 'flex-end',
    right: 65,
    position: 'absolute',
    zIndex: 3,
  },
  dangerCircle: {
    width: 29,
    height: 29,
    backgroundColor: 'white',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dangerIcon: {
    width: 18,
    height: 16,
  },
  drawerContainer: {
    position: 'absolute',
    marginTop: 60,
    marginLeft: 24,
    flexDirection: 'row',
    width: '100%',
    zIndex: 5,
  },
  drawerIcon: {
    width: 18,
    height: 11,
  },
  topImage: {
    width: '100%',
    height: 249,
    position: 'relative',
  },
  headerContainer: {
    width: 200,
    height: 50,
    alignSelf: 'center',
    marginTop: 60,
    position: 'absolute',
  },
  textHeader: {
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  mainTab: {
    width: '100%',
    backgroundColor: 'white',
    height: 50,
    position: 'absolute',
    bottom: 10,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    justifyContent: 'center',
    paddingHorizontal: 70,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 16,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
});
