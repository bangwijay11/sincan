import {
  Alert,
  Button,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {deleteMateri, postMateri} from '../Stores/Actions/ATolongAction';
import {useDispatch, useSelector} from 'react-redux';

import Axios from 'axios';
import {KOS_URL} from '@env';
import {Textarea} from 'native-base';
import images from '../../../app/config/images';

const VItem = ({Judul, desc, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Image source={images.sincan} style={styles.avatar} />
      </TouchableOpacity>
      <View style={styles.desc}>
        <Text style={styles.descName}>{Judul}</Text>
        <Text style={styles.descBidang}>{desc}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>x</Text>
      </TouchableOpacity>
    </View>
  );
};
const ATolongComponent = () => {
  const dispatch = useDispatch();
  const [Judul, setJudul] = useState('');
  const [isi, setIsi] = useState('');
  const [desc, setDesc] = useState('');
  const [materi, setMateri] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});
  const [] = useState('key0');
  useEffect(() => {
    getData();
  }, []);
  const submit = () => {
    const data = {
      Judul,
      isi,
      desc,
    };

    if (Judul && isi && materi != '') {
      if (button === 'Simpan') {
        dispatch(postMateri(data));
        console.log('data dikirim');
        setJudul(''), setIsi(''), setDesc('');
        getData();
      } else if (button === 'Update') {
        Axios.put(`${KOS_URL}/Pertolongan/${selectedUser.id}`, data).then(
          () => {
            setButton('Simpan');
            setJudul(''), setIsi(''), setDesc('');
            getData();
          },
        );
      }
    } else {
      Alert.alert('Data tidak boleh kosong Min !');
    }
  };

  const getData = () => {
    Axios.get(`${KOS_URL}/Pertolongan/`).then((res) => {
      console.log('data', res);
      setMateri(res.data);
    });
  };
  const selectItem = (item) => {
    console.log('selected item', item);
    setSelectedUser(item);
    setJudul(item.Judul);
    setDesc(item.desc);
    setIsi(item.isi);
    setButton('Update');
  };
  const deleteItem = (item) => {
    console.log(item);
    dispatch(deleteMateri(item));
    getData();
  };
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.textTitle}>Dashboard Admin</Text>
        <Text style={styles.textTitle}> Masukkan Data Materi </Text>
        <View style={{marginTop: 30}}>
          <TextInput
            placeholder="Judul"
            style={styles.input}
            value={Judul}
            onChangeText={(value) => setJudul(value)}
          />
          <TextInput
            placeholder="Deskripsi"
            style={styles.input}
            value={desc}
            onChangeText={(value) => setDesc(value)}
          />
          <Textarea
            placeholder="Isi"
            style={[styles.input, {height: 100}]}
            value={isi}
            onChangeText={(value) => setIsi(value)}
          />
        </View>
        <Button title={button} onPress={submit} />
        <View style={styles.line} />
        {materi.map((materi) => {
          return (
            <VItem
              key={materi.id}
              Judul={materi.Judul}
              desc={materi.desc}
              isi={materi.isi}
              onPress={() => selectItem(materi)}
              onDelete={() =>
                Alert.alert(
                  'Peringatan',
                  'anda yakin akan menghapus user ini ?',
                  [
                    {
                      text: 'Tidak',
                      onPress: () => console.log('button tidak'),
                    },
                    {
                      text: 'YA',
                      onPress: () => deleteItem(materi),
                    },
                  ],
                )
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default ATolongComponent;
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
  line: {
    height: 2,
    backgroundColor: 'grey',
    marginVertical: 20,
  },
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 20,
  },
  itemContainer: {flexDirection: 'row', marginBottom: 8},
  desc: {marginLeft: 18, flex: 2},
  descName: {fontSize: 18, fontWeight: 'bold', color: 'red'},
  descEmail: {fontSize: 16},
  descBidang: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 20, color: 'red'},
});
