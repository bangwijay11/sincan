import { Text, View } from 'react-native'

import ATolongComponent from '../Component/ATolongComponent'
import React from 'react'

const ATolongContainer = (props) => {
    return (
       <ATolongComponent
           {...props}
       />

    )
}

export default ATolongContainer
