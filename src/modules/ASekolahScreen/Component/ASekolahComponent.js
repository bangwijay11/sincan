import {
  Alert,
  Button,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Item, Picker, Textarea} from 'native-base';
import React, {useEffect, useState} from 'react';
import {
  deleteMateriSiaga,
  postMateriSiaga,
} from '../Stores/Actions/ASekolahAction';
import {useDispatch, useSelector} from 'react-redux';

import Axios from 'axios';
import {KOS_URL} from '@env';
import images from '../../../app/config/images';

const VItem = ({judul, isi, desc, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Image source={images.sincan} style={styles.avatar} />
      </TouchableOpacity>
      <View style={styles.desc}>
        <Text style={styles.descName}>{judul}</Text>
        <Text style={styles.descBidang}>{desc}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>x</Text>
      </TouchableOpacity>
    </View>
  );
};
const ASekolahComponent = (props) => {
  const dispatch = useDispatch();
  const [judul, setJudul] = useState('');
  const [isi, setIsi] = useState('');
  const [desc, setDesc] = useState('');
  const [sekolah, setSekolah] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});
  const [topik, setTopik] = useState('key0');
  useEffect(() => {
    getData();
  }, []);
  const submit = () => {
    const data = {
      judul,
      isi,
      desc,
    };

    if (judul && isi && desc !== '') {
      if (button === 'Simpan') {
        dispatch(postMateriSiaga(data));
        console.log('data dikirim');
        setJudul(''), setIsi(''), setDesc('');
        getData();
      } else if (button === 'Update') {
        Axios.put(`${KOS_URL}/sekolah/${selectedUser.id}`, data).then((res) => {
          setButton('Simpan');
          setJudul(''), setIsi(''), setDesc('');
          getData();
        });
      }
    } else {
      Alert.alert('Data tidak boleh kosong Min !');
    }
  };

  const getData = () => {
    Axios.get(`${KOS_URL}/sekolah/`).then((res) => {
      console.log('data', res);
      setSekolah(res.data);
    });
  };
  const selectItem = (item) => {
    console.log('selected item', item);
    setSelectedUser(item);
    setJudul(item.judul);
    setDesc(item.desc);
    setIsi(item.isi);
    setButton('Update');
  };
  const deleteItem = (item) => {
    console.log(item);
    dispatch(deleteMateriSiaga(item));
    getData();
  };
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.textTitle}>Dashboard Admin</Text>
        <Text style={styles.textTitle}>
          {' '}
          Masukkan Data Materi Sekolah Siaga{' '}
        </Text>
        <View style={{marginTop: 30}}>
          <TextInput
            placeholder="judul"
            style={styles.input}
            value={judul}
            onChangeText={(value) => setJudul(value)}
          />
          <TextInput
            placeholder="Deskripsi"
            style={styles.input}
            value={desc}
            onChangeText={(value) => setDesc(value)}
          />
          <Textarea
            placeholder="Isi"
            style={[styles.input, {height: 100}]}
            value={isi}
            onChangeText={(value) => setIsi(value)}
          />
        </View>
        <Button title={button} onPress={submit} />
        <View style={styles.line} />
        {sekolah.map((sekolah) => {
          return (
            <VItem
              key={sekolah.id}
              judul={sekolah.judul}
              desc={sekolah.desc}
              isi={sekolah.isi}
              onPress={() => selectItem(sekolah)}
              onDelete={() =>
                Alert.alert(
                  'Peringatan',
                  'anda yakin akan menghapus user ini ?',
                  [
                    {
                      text: 'Tidak',
                      onPress: () => console.log('button tidak'),
                    },
                    {
                      text: 'YA',
                      onPress: () => deleteItem(sekolah),
                    },
                  ],
                )
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default ASekolahComponent;
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
  line: {
    height: 2,
    backgroundColor: 'grey',
    marginVertical: 20,
  },
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 20,
  },
  itemContainer: {flexDirection: 'row', marginBottom: 8},
  desc: {marginLeft: 18, flex: 2},
  descName: {fontSize: 18, fontWeight: 'bold', color: 'red'},
  descEmail: {fontSize: 16},
  descBidang: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 20, color: 'red'},
});
