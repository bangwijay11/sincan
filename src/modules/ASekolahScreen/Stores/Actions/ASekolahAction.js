import {
  ADD_MATERI_SUCCESS,
  DELETE_MATERI_ERROR,
  DELETE_MATERI_SUCCESS,
  SET_MATERI,
  UPDATE_MATERI_ERROR,
  UPDATE_MATERI_SUCCESS,
} from '../../../../app/store/redux/action';

import Axios from 'axios';
import {KOS_URL} from '@env';
import fetch from 'cross-fetch';

export const addMateriSuccess = (payload) => ({
  type: ADD_MATERI_SUCCESS,
  payload,
});

export const addMateriError = (error) => ({
  type: ADD_MATERI_ERROR,
  error,
});

export const deleteMateriSuccess = () => ({
  type: DELETE_MATERI_SUCCESS,
});

export const deleteMateriError = (error) => ({
  type: DELETE_MATERI_ERROR,
  error,
});

export const setMateriInput = (inputType, value) => {
  return {type: SET_MATERI, inputType: inputType, inputValue: value};
};

export const postMateriSiaga = (data) => {
  return (dispatch) => {
    Axios.post(`${KOS_URL}/sekolah`, data)
      .then((res) => {
        dispatch({
          type: 'ADD_MATERI',
          payload: data,
        });
        if (!data.error) {
          dispatch(addMateriSuccess(data));
        } else {
          dispatch(addMateriError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const deleteMateriSiaga = (item) => {
  return (dispatch) => {
    Axios.delete(`${KOS_URL}/sekolah/${item.id}`)
      .then((res) => {
        dispatch({
          type: 'DELETE_MATERI',
        });

        if (!data.error) {
          dispatch(deleteMateriSuccess());
        } else {
          dispatch(deleteMateriError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
