import { Text, View } from 'react-native'

import ASekolahComponent from '../Component/ASekolahComponent'
import React from 'react'

const ASekolahContainer = (props) => {
    return (
        <ASekolahComponent
            {...props}
        />
    )
}

export default ASekolahContainer
