import {
  Alert,
  Button,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  deleteMateri,
  postMateri,
} from '../../AMateriScreen/Stores/Actions/AMateriAction';
import {deleteNews, postNews} from '../Stores/Actions/ABeritaAction';
import {useDispatch, useSelector} from 'react-redux';

import Axios from 'axios';
import {KOS_URL} from '@env';
import {Textarea} from 'native-base';
import images from '../../../app/config/images';

const Item = ({judul, desc, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Image source={images.sincan} style={styles.avatar} />
      </TouchableOpacity>
      <View style={styles.desc}>
        <Text style={styles.descName}>{judul}</Text>
        <Text style={styles.descBidang}>{desc}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>x</Text>
      </TouchableOpacity>
    </View>
  );
};
const ABeritaComponent = () => {
  const dispatch = useDispatch();
  const [judul, setJudul] = useState('');
  const [statiun, setStatiun] = useState('');
  const [status, setStatus] = useState('');
  const [photo, setPhoto] = useState(
    'https://asset.kompas.com/crops/v_lkgrVAqTclvW57CvpgaP0sgLc=/17x0:934x611/750x500/data/photo/2020/09/25/5f6dbdc217290.jpg',
  );
  const [isi, setIsi] = useState('');
  const [desc, setDesc] = useState('');
  const [news, setNews] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});
  useEffect(() => {
    getData();
  }, []);
  const submit = () => {
    const data = {
      judul,
      isi,
      desc,
      photo,
      statiun,
      status,
    };

    if (judul && isi && desc != '') {
      if (button === 'Simpan') {
        dispatch(postNews(data));
        console.log('data dikirim');
        setJudul(''), setIsi(''), setDesc('');
        setStatiun('');
        setStatus('');
        getData();
      } else if (button === 'Update') {
        Axios.put(`${KOS_URL}/news/${selectedUser.id}`, data).then(() => {
          setButton('Simpan');
          setJudul(''), setIsi(''), setDesc('');
          setStatiun('');
          setStatus('');
          getData();
        });
      }
    } else {
      Alert.alert('Data tidak boleh kosong Min !');
    }
  };

  const getData = () => {
    Axios.get(`${KOS_URL}/news/`).then((res) => {
      console.log('data', res);
      setNews(res.data);
    });
  };
  const selectItem = (item) => {
    console.log('selected item', item);
    setSelectedUser(item);
    setJudul(item.judul);
    setDesc(item.desc);
    setIsi(item.isi);
    setStatiun(item.statiun);
    setStatus(item.status);
    setButton('Update');
  };
  const deleteItem = (item) => {
    console.log(item);
    dispatch(deleteNews(item));
    getData();
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.textTitle}>Dashboard Admin</Text>
        <Text style={styles.textTitle}> Masukkan Berita </Text>
        <View style={{marginTop: 30}}>
          <TextInput
            placeholder="Judul"
            style={styles.input}
            value={judul}
            onChangeText={(value) => setJudul(value)}
          />
          <TextInput
            placeholder="Stasiun"
            style={styles.input}
            value={statiun}
            onChangeText={(value) => setStatiun(value)}
          />
          <TextInput
            placeholder="Photo"
            style={styles.input}
            value={photo}
            onChangeText={(value) => setPhoto(value)}
          />
          <TextInput
            placeholder="Populer/Headline"
            style={styles.input}
            value={status}
            onChangeText={(value) => setStatus(value)}
          />
          <TextInput
            placeholder="Deskripsi"
            style={styles.input}
            value={desc}
            onChangeText={(value) => setDesc(value)}
          />
          <Textarea
            placeholder="Isi"
            style={[styles.input, {height: 100}]}
            value={isi}
            onChangeText={(value) => setIsi(value)}
          />
        </View>
        <Button title={button} onPress={submit} />
        <View style={styles.line} />
        {news.map((news) => {
          return (
            <Item
              key={news.id}
              judul={news.judul}
              desc={news.desc}
              photo={news.photo}
              statiun={news.statiun}
              isi={news.isi}
              onPress={() => selectItem(news)}
              onDelete={() =>
                Alert.alert(
                  'Peringatan',
                  'anda yakin akan menghapus user ini ?',
                  [
                    {
                      text: 'Tidak',
                      onPress: () => console.log('button tidak'),
                    },
                    {
                      text: 'YA',
                      onPress: () => deleteItem(news),
                    },
                  ],
                )
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default ABeritaComponent;
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
  line: {
    height: 2,
    backgroundColor: 'grey',
    marginVertical: 20,
  },
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 20,
  },
  itemContainer: {flexDirection: 'row', marginBottom: 8},
  desc: {marginLeft: 18, flex: 2},
  descName: {fontSize: 18, fontWeight: 'bold', color: 'red'},
  descEmail: {fontSize: 16},
  descBidang: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 20, color: 'red'},
});
