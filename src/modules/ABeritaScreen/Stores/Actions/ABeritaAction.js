import {
  ADD_MATERI_ERROR,
  ADD_MATERI_SUCCESS,
} from '../../../../app/store/redux/action';

import Axios from 'axios';
import {KOS_URL} from '@env';

export const addNewsSuccess = (payload) => ({
  type: ADD_MATERI_SUCCESS,
  payload,
});

export const addNewsError = (error) => ({
  type: ADD_MATERI_ERROR,
  error,
});

export const deleteNewsSuccess = (payload) => ({
  type: DELETE_NEWS_SUCCESS,
  payload,
});

export const deleteNewsError = (error) => ({
  type: DELETE_NEWS_ERROR,
  error,
});

export const postNews = (data) => {
  return (dispatch) => {
    Axios.post(`${KOS_URL}/news`, data)
      .then((res) => {
        dispatch({
          type: 'ADD_MATERI',
          payload: data,
        });
        if (!data.error) {
          dispatch(addNewsSuccess(data));
        } else {
          dispatch(addNewsError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const deleteNews = (item) => {
  return (dispatch) => {
    Axios.delete(`${KOS_URL}/news/${item.id}`)
      .then((res) => {
        dispatch({
          type: 'DELETE_NEWS',
          payload: data,
        });

        if (!data.error) {
          dispatch(deleteNewsSuccess(data));
        } else {
          dispatch(deleteNewsError(data.error));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
