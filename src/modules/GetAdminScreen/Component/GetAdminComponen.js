import React, { useEffect, useState } from 'react'
import { Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { Alert } from 'react-native'
import Axios from 'axios'
import { BackIcon } from '../../../property'
import { Colors } from '../../../property/colors/colors'
import { NewsCard } from '../../../components'
import { ScrollView } from 'react-native'
import { StatusBar } from 'react-native'
import { StyleSheet } from 'react-native'
import { TextInput } from 'react-native'
import { TouchableHighlight } from 'react-native'
import { TouchableOpacity } from 'react-native'
import { getNews } from '../Stores/Actions/GetAdminAction'

const GetAdminComponen = (props) => {
    const [quiz, setQuiz] = useState([])
    const [index, setIndex] = useState(0)
    const [click, setClick] = useState(false)
    const [selectedUser, setSelectedUser] = useState({});
    const [score, setScore] = useState({
        correct: 0,
        false: 0
    })
    const getNews = () => {
        Axios.get('http://192.168.43.41:3000/Soal/')
            .then(res => {
                console.log('ujian', res.data[index]);
                setQuiz(res.data)
            })
    }
    useEffect(() => {
        getNews()
    }, []);

    const selectItems = (item) => {
        console.log('selected', item.correct);
        setSelectedUser(item);
        console.log('select', click);
        setClick({ click: true })
        if (item.correct == true) {
            setScore({
                correct: score.correct + 1
            })
            console.log('brtul', score.correct);
        } else {
            console.log('salah');
        }
    }

    const result = 20 * score.correct
    console.log('hasil', result);
    const hasil = () => {
        Alert.alert('hasil', result)
    }

    return (
        <View>
            <StatusBar backgroundColor="transparent" translucent />
            <ScrollView>
                <View style={{
                    width: '100%',
                    backgroundColor: '#6FC9C0',
                    height: 80,
                    borderBottomLeftRadius: 15,
                    borderBottomRightRadius: 15,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: 18,

                }}>
                    <View>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <BackIcon width={24} height={24} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>
                        Quiz
                </Text>
                    <View style={{
                        width: 50, height: 50, borderRadius: 25, backgroundColor: 'grey', marginTop: 15
                    }}>

                    </View>

                </View>
                <View style={{ paddingHorizontal: 18, marginTop: 20 }}>

                    {quiz.map((quiz, key) => {
                        return (
                            <View key={key}>
                                <View style={styles.question}>
                                    <Text style={{ color: 'white', fontSize: 15 }}>{quiz.question}</Text>
                                </View>
                                { quiz.Options.map((optio, key2) => {
                                return <TouchableOpacity key={key2}>
                                    <TouchableOpacity style={[styles.Options]} onPress={() => selectItems(optio)} >
                                        <Text style={styles.text}>{optio.title}</Text>  
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            })}
                                
                            </View>
                        )
                    })}
                    <TouchableOpacity onPress={() => hasil}>
                        <Text>hasil {result}</Text>
                    </TouchableOpacity>
                    <View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}


export default GetAdminComponen

const styles = StyleSheet.create({
    // click ? {backgroundColor:"white"} : {backgroundColor: '#6FC9C0'}
    content: {
        paddingHorizontal: 20,
        borderRadius: 25
    },
    Options: {
        width: '95%', padding: 10,
        backgroundColor:"#6FC9C0" ,
        marginBottom: 10,
        marginRight: 5,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    text: {
        color: 'white',
        padding: 10,
        textAlignVertical: 'center'
    },
    question: {
        width: "100%",
        height: 170,
        backgroundColor: '#6FC9C0',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 40
    }
})