import {FETCH_NEWS, FETCH_NEWS_ERROR, FETCH_NEWS_SUCCESS} from '../../../../app/store/redux/action'

import Axios from 'axios'
import fetch  from 'cross-fetch'

export const fetchNews  = () => ({
    type: FETCH_NEWS,
})
export const fetchNewsSuccess  = (payload) => ({
    type: FETCH_NEWS_SUCCESS ,
    payload
})
export const fetchNewsError  = error => ({
    type: FETCH_NEWS_ERROR,
    error

})

export const callApi = async(URL) => {
    try {
        const res = await fetch(URL);
        
        if (res.status >= 400) {
          throw new Error("Bad response from server");
        }
        
        const data = await res.json();
      
        console.log(data);
        return data;
      } catch (err) {
        return {error: err.message};
      }
    
}


export const getNews = () =>  dispatch => {
    dispatch(fetchNews())
    callApi('http://192.168.43.41:3000/Soal/'). then(data => {
        if(!data.error){
            dispatch(fetchNewsSuccess(data))
        }else{
            dispatch(fetchNewsError(data.error))
        }
    })
}

