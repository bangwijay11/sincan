import { Text, View } from 'react-native'

import AdminComponent from '../Component/AdminComponent'
import React from 'react'

const AdminContainer = (props) => {
    const detect = () => {
        const today = new Date()
         const hour = today.getHours();
        if (hour < 12){
          return ('Selamat Pagi');                    
        } else if (hour < 18){
          return('selamat siang');
        } 
        return ('Selamat Malam')
      }

    return (
        <AdminComponent 
            move={() => props.navigation.navigate('AMateri')}
            moveNews={() => props.navigation.navigate('ANews')}
            moveHelp = {() => props.navigation.navigate('ATolong')}
            moveSiaga = {() => props.navigation.navigate('ASekolah')}
            moveNo = {() => props.navigation.navigate('ANumber')}
            greet={detect()}
            {...props}
         />
    )
}

export default AdminContainer
