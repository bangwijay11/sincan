import { BarStatus, BoxAdmin } from '../../../components'
import { Text, View } from 'react-native'

import { Colors } from '../../../property/colors/colors'
import { Image } from 'react-native'
import React from 'react'
import { SUBTITLE_COURSE } from '../../../components/FontStyle/FontStyle'
import { StyleSheet } from 'react-native'
import images from '../../../app/config/images'

const AdminComponent = (props) => {
    return (
        <View>
            <BarStatus />
            <View style={styles.header}>
                <View style={styles.ctnTxt} >
                    <Text style={SUBTITLE_COURSE}>Hai, {props.greet} Min</Text>
                </View>
                <View style={styles.ctnImg}>
                    <Image source={images.sincan} style={styles.img} />
                </View>
            </View>
            <View style={styles.contentCtn}>
                <View style={styles.content}>
                    <BoxAdmin
                        title="Materi"
                        move={props.move}
                    />
                    <BoxAdmin
                        title="Materi Pertolongan" /// beda sam materi bub == judul
                        move={props.moveHelp}
                    />
                    <BoxAdmin
                        title="Materi Sekolah Siaga" // sama kaya pertolongan
                        move={props.moveSiaga}
                    />
                    <BoxAdmin
                        title="Berita" // status, judul, stasiun, photo, desc,
                        move={props.moveNews}
                    />
                   <BoxAdmin
                        title="Nomor Darurat"
                        move={props.moveNo}
                    />
                </View>
            </View>
        </View>
    )
}

export default AdminComponent
const styles = StyleSheet.create({
    box: {
        width: 150,
        height: 90,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primary,
        borderRadius: 25,
        shadowColor: "#000",
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    ctnTxt: {
        paddingLeft: 20,
        paddingTop: 30
    },
    contentCtn: {
        paddingHorizontal: 20
    },
    content: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 40,
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    header: {
        width: '100%',
        height: 95,
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    ctnImg: {
        paddingTop: 18
    },
    img: {
        width: 50,
        height: 50
    },
    txtBox: {
        color: Colors.font,
        fontSize: 16
    }
})
