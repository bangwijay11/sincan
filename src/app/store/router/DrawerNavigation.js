import CHelpContainer from '../../../modules/CourseHelpScreen/Container/CHelpContainer';
import CSchoolContainer from '../../../modules/CourseSchool/Container/CSchoolContainer';
import ClassContainer from '../../../modules/ClassScreen/Container/ClassContainer';
import CourseContainer from '../../../modules/CourseScreen/Container/CourseContainer';
import { DrawerContent } from '../../../components';
import HelpContainer from '../../../modules/PertolonganScreen/Container/HelpContainer';
import LessonContainer from '../../../modules/LessonScreen/Container/LessonContainer';
import NumberContainer from '../../../modules/NumberScreen/Container/NumberContainer';
import QuizContainer from '../../../modules/QuizScreen/Container/QuizContainer';
import React from 'react';
import ResultComponent from '../../../modules/ResultScreen/Component/ResultComponent';
import SekolahScreen from '../../../modules/SekolahScreen/Component/SekolahScreen';
import TobBottomNavigation from './TabBottomNavigation';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

const Drawer = createDrawerNavigator();
const HomeStack = createStackNavigator();


const DrawerNavigation = () => {
    return (
    <Drawer.Navigator 
    drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="Home" component={HomeStackScreen} />
    </Drawer.Navigator>
    )
}

export default DrawerNavigation;



const HomeStackScreen = () => (
    <HomeStack.Navigator initialRouteName="Home">
    
        <HomeStack.Screen 
            name="Home" component={TobBottomNavigation}
            options={{
                headerShown: false,
            }}
        />
         <HomeStack.Screen 
            name="Class" component={ClassContainer}
            options={{
                headerShown: false,
            }}
        />
        <HomeStack.Screen 
            name="Result" component={ResultComponent}
            options={{
                headerShown: false,
            }}
        />
         <HomeStack.Screen 
            name="Course" component={CourseContainer}
            options={{
                headerShown: false,
            }}
        />
         <HomeStack.Screen 
            name="Done" component={LessonContainer}
            options={{
                headerShown: false,
            }}
        />
         <HomeStack.Screen 
            name="Pertolongan" component={HelpContainer}
            options={{
                headerShown: false
            }}
        />
         <HomeStack.Screen 
            name="Sekolah" component={SekolahScreen}
            options={{
                headerShown: false
            }}
        />
         <HomeStack.Screen 
            name="CHelp" component={CHelpContainer}
            options={{
                headerShown: false
            }}
        />
         <HomeStack.Screen 
            name="CSchool" component={CSchoolContainer}
            options={{
                headerShown: false
            }}
        />
         <HomeStack.Screen 
            name="Number" component={NumberContainer}
            options={{
                headerShown: true
            }}
        />
         <HomeStack.Screen 
            name="Quiz" component={QuizContainer}
            options={{
                headerShown: false
            }}
        />
        
    </HomeStack.Navigator>
    
);