import LoginContainer from '../../../modules/LoginScreen/Container/LoginContainer';
import OnBoardingContainer from '../../../modules/OnBoarding/Container/OnBoardingContainer';
import React from 'react'
import RegisterContainer from '../../../modules/RegisterScreen/Container/RegisterContainer';
import SplashContainer from '../../../modules/SplashScreen/Container/SplashContainer';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const RootStckNavigator = () => {
    return (
        <Stack.Navigator initialRouteName="OnBoarding">
            <Stack.Screen name="Splash" component={SplashContainer} options={{
                headerShown: false
            }} />
            <Stack.Screen name="OnBoarding" component={OnBoardingContainer} options={{
                headerShown: false
            }} />
            <Stack.Screen name="Login" component={LoginContainer} options={{
                headerShown: false
            }} />
            <Stack.Screen name="Register" component={RegisterContainer} options={{
                headerShown: false
            }} />

        </Stack.Navigator>
    )
}

export default RootStckNavigator
