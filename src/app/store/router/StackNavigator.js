import ABeritaContainer from '../../../modules/ABeritaScreen/Container/ABeritaContainer';
import AMateriContainer from '../../../modules/AMateriScreen/Container/AMateriContainer';
import ANumberContainer from '../../../modules/ANumberScreen/Container/ANumberContainer';
import ASekolahContainer from '../../../modules/ASekolahScreen/Container/ASekolahContainer';
import ATolongContainer from '../../../modules/ATolongScreen/Container/ATolongContainer';
import AdminContainer from '../../../modules/AdminScreen/Container/AdminContainer';
import DrawerNavigation from './DrawerNavigation';
import GetAdminComponen from '../../../modules/GetAdminScreen/Component/GetAdminComponen';
import LoginContainer from '../../../modules/LoginScreen/Container/LoginContainer';
import OnBoardingContainer from '../../../modules/OnBoarding/Container/OnBoardingContainer';
import React from 'react';
import RegisterContainer from '../../../modules/RegisterScreen/Container/RegisterContainer';
import RootStckNavigator from './RootStckNavigator';
import SplashContainer from '../../../modules/SplashScreen/Container/SplashContainer';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <>
      <Stack.Navigator initialRouteName="Splash">
        {/* <Stack.Screen name="Root" component={RootStckNavigator} options={{
          headerShown: false
        }} /> */}
        <Stack.Screen
          name="Splash"
          component={SplashContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="OnBoarding"
          component={OnBoardingContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Login"
          component={LoginContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Register"
          component={RegisterContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Home"
          component={DrawerNavigation}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Admin"
          component={AdminContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AMateri"
          component={AMateriContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="GetA"
          component={GetAdminComponen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ANews"
          component={ABeritaContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ATolong"
          component={ATolongContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ASekolah"
          component={ASekolahContainer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ANumber"
          component={ANumberContainer}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </>
  );
};
export default StackNavigator;
// isLoggin
