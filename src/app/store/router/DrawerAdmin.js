import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import AdminContainer from '../../../modules/AdminScreen/Container/AdminContainer';

const Drawer = createDrawerNavigator();
const AdminStack = createStackNavigator();

const DrawerAdmin = () => {
    return (
    <Drawer.Navigator 
    drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="Home" component={HomeStackScreen} />
    </Drawer.Navigator>
    )
}

export default DrawerAdmin;

const AdminStack = () => (
    <AdminStack.Navigator initialRouteName="Admin">
        <AdminStack.Screen
        name="Admin" component={AdminContainer}
        options={{headerShown:false,
        }}
         />
    </AdminStack.Navigator>
)