import ClassScreen from '../../../modules/ClassScreen/Component/CLassScreen';
import { Colors } from '../../../property/colors/colors';
import DetailNewsContainer from '../../../modules/DetailNewsScreen/Container/DetailNewsContainer';
import HelpContainer from '../../../modules/PertolonganScreen/Container/HelpContainer';
import HomeContainer from '../../../modules/HomeScreen/Container/HomeContainer';
import Icon from 'react-native-vector-icons/Ionicons';
import MapScreen from '../../../modules/MapScreen/Component/MapScreen';
import NewsContainer from '../../../modules/NewsScreen/Container/NewsContainer';
import React from 'react';
import SubClassContainer from '../../../modules/SubClassScreen/Container/SubClassContainer';
import TsunamiContainer from '../../../modules/TsunamiScreen/Container/TsunamiContainer';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

const HomeStack = createStackNavigator();
const NewsStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const TobBottomNavigation = () => (
  <Tab.Navigator 
  
  inactiveColor="#D2D2D2"
  barStyle={{backgroundColor: '#FFFFFF'}}
  initialRouteName="Home" 
  activeColor="#6FC9C0">
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color}) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="News"
      component={NewsStackScreen}
      options={{
        tabBarLabel: 'News',
        tabBarIcon: ({color}) => (
          <Icon name="book" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Maps"
      component={MapScreen}
      options={{
        tabBarLabel: 'Maps',
        tabBarIcon: ({color}) => (
          <Icon name="location" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default TobBottomNavigation;

const HomeStackScreen = () => (
  <HomeStack.Navigator initialRouteName="Home">
    <HomeStack.Screen
      name="Home"
      component={HomeContainer}
      options={{
        headerShown: false,
        headerTransparent: true,
        headerTitle: false
      }}
    />
    <HomeStack.Screen
      name="Class"
      component={ClassScreen}
      options={{
        headerShown: false,
      }}
    />
    <HomeStack.Screen
      name="SubClass"
      component={SubClassContainer}
      options={{
        headerShown: false,
      }}
    />
    <HomeStack.Screen
      name="Pertolongan"
      component={HelpContainer}
      options={{
        headerShown: false,
      }}
    />
    <HomeStack.Screen
      name="Tsunami"
      component={TsunamiContainer}
      options={{
        headerShown: false,
      }}
    />
  </HomeStack.Navigator>
);

const NewsStackScreen = () => (
  <NewsStack.Navigator initialRouteName="News"
  >
    <NewsStack.Screen
      name="News"
      component={NewsContainer}
      options={{
        headerShown: false
      }}
    />
    <NewsStack.Screen
     name="Detail"
      component={DetailNewsContainer}
      options={{
        headerShown: false,
      }}
    />
  </NewsStack.Navigator>
);
