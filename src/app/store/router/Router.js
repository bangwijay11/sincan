import React from 'react';
import StackNavigator from './StackNavigator';
import {createStackNavigator} from '@react-navigation/stack';

const Router = () => {
  return (
    <>
      <StackNavigator/>
    </>
  );
};
export default Router;