import {applyMiddleware, createStore} from 'redux';

import {createLogger} from 'redux-logger'
import reducer from './reducer';
import thunk from 'redux-thunk';

const middleware = applyMiddleware(thunk, createLogger());

const store = createStore(reducer, middleware);

export default store;