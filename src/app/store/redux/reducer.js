import AMateriReducer from '../../../modules/AMateriScreen/Stores/Reducers/AMateriReducer';
import ClassReducer from '../../../modules/ClassScreen/Store/Reducers/ClassReducer';
import GetAdminReducer from '../../../modules/GetAdminScreen/Stores/Reducers/GetAdminReducer';
import LoginReducer from '../../../modules/LoginScreen/Stores/Reducers/LoginReducer';
import NewsReducer from '../../../modules/NewsScreen/store/Reducer/NewsReducer';
import NumberReducer from '../../../modules/NumberScreen/Stores/Reducers/NumberReducer';
import PertolonganReducer from '../../../modules/PertolonganScreen/Stores/Reducers/PertolonganReducer';
import QuizReducer from '../../../modules/QuizScreen/Stores/Reducers/QuizReducer';
import RegisterReducer from '../../../modules/RegisterScreen/Stores/Reducers/RegisterReducer';
import SekolahReducer from '../../../modules/SekolahScreen/Stores/Reducers/SekolahReducer';
import ListProdukReducer from '../../../modules/HomeScreen/Stores/Reducers/HomeReducers';
import { combineReducers } from 'redux';

const reducer = combineReducers({
  RegisterReducer,
  AMateriReducer,
  LoginReducer,
  QuizReducer,
  ClassReducer,
  PertolonganReducer,
  NumberReducer,
  SekolahReducer,
  GetAdminReducer,
  NewsReducer,
  ListProdukReducer,
});

export default reducer;
