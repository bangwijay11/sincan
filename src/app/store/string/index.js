const MATERI_STRING = {
    // Sekolah Siaga 
    SEKOLAH_PENGERTIAN: 
    'Sekolah Siaga Bencana (SSB) merupakan upaya membangun kesiapsiagaan sekolah terhadap bencana dalam rangka menggugah kesadaran seluruh unsur-unsur dalam bidang pendidikan baik individu maupun kolektif di sekolah dan lingkungan sekolah baik itu sebelum, saat maupun setelah bencana terjadi.',
    SEKOLAH_TUJUAN: 
    '-Membangun budaya siaga dan budaya aman disekolah dengan mengembangkan jejaring bersama para pemangku kepentingan di bidang penanganan bencana; Meningkatkan kapasitas institusi sekolah dan individu dalam mewujudkan tempat belajar yang lebih aman bagi siswa, guru, anggota komunitas sekolah serta komunitas di sekeliling sekolah; Menyebarluaskan dan mengembangkan pengetahuan kebencanaan ke masyarakat luas melalui jalur pendidikan sekolah',
    SEKOLAH_LANGKAH: 
    '1. Membangun kesepahaman & komitmen bersama antar anggota komunitas sekolah dengan atau tanpa difasilitasi oleh pihak luar. 2. Membuat rencana aksi bersama antara sekolah, komite sekolah, orang tua, dan anak-anak (bisa dalam bentuk lokakarya, FGD,atau pertemuan rutin). 3. Melakukan kajian tingkat kesiagaan sekolah dengan menggunakan lima parameter (pengetahuan dan sikap, kebijakan, rencana tanggap darurat, sistem peringatan dini, dan mobilisasi sumberdaya).Peningkatan kapasitas (pelatihan-pelatihan) untuk semua stakeholder sekolah (guru, karyawan/staf administrasi, satuan pengamanan, anggota komite sekolah, orang tua, anak-anak).',
    SEKOLAH_INDIKATOR: 
    '1. Adanya dokumen penilaian risiko bencana yang disusun bersama secara partisipatif dengan warga sekolah dan pemangku kepentingan sekolah. 2. Adanya protokol komunikasi dan koordinasi.3. Adanya Prosedur Tetap Kesiagaan Sekolah yang disepakati dan dilaksanakan oleh seluruh komponen sekolah',
    // menu home
    BELAJAR: 
    'Siaga Belajar',
    SOAL:
    'Latihan Soal',
    PERTOLONGAN: 
    'Siaga Pertolongan',
    SEKOLAH:
    'Menuju  Sekolah Bencana',
    // CLASS
    PEMBELAJARAN: 
    'Pembelajaran Siswa',
    GEMPA: 
    'Gempa',
    DESC_GEMPA:
    'Apa itu gempa bumi? Mari kita pelajari !!',
    TSUNAMI:
    'Tsunami',
    DESC_TSUNAMI:
    'Apa itu Tsunami? Mari kita pelajari !!',
    //PERTOLONGAN
    PERTOLONGAN_PERTAMA: 
    'Pertolongan Pertama',
    LUKA_TERBUKA:
    'Luka Terbuka' ,
    DESC_LUKA:
    'Apa si yang harus dilakukan untuk mengobati luka terbuka?',
    PATAH:
    'Patah Tulang',
    DESC_PATAH:
    'Apa si yang pertamakali kita dilakukan ketika patah tulang?',
    BENDA_ASING:
    'Luka Akibat Benda Asing',
    DESC_ASING:
    'Apa si yang harus dilakukan untuk mengobati luka terbuka?',
    // Done
    SELAMAT:
    'Selamat',
    DESC_SELAMAT: 
    'Kamu telah menyelesaikan pembelajaran ini',
    DESC_LANJUT: 
    'Ayo lanjutkan lagi belajarnya..Semangatt...!',
    DONE: 
    'SELESAI',
    

    
}

export default MATERI_STRING