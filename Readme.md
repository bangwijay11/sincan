Aplikasi sincan adalah aplikasi pembelajaran tentang bencana alam, didalamnya terdapat beberapa fitur :

# Authentication dengan firebase

- Registrasi
- Login dengan email & password
- Login dengan google

# Module Pembelajaran

- Berisikan materi tentang bencana alam
- Berisikan materi tentang pertolongan pertama saat bencana alam

# Quiz

- Berisikan Soal-soal tentang bencana alam yang bertujuan untuk memperdalam pemahaman pengguna tentang bencana alam

# News

- Berisi Berita bencana alam
- User bisa menshare berita melalui medsos(fb,ig,wa,telegram dll.)

# Maps

- Menunjukkan lokasi kita berada
- Menunjukkan tempat darurat yg ada di sekitar kita(Rumah sakit, Pemadam Kebakaran dll.)

# Telfon darurat

- Berisi Profil dari tempat darurat beserta alamat & nomor telfon yang bisa ditelfon jika diperlukan
