/**
 * @format
 */

import App from './src/App.js';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import database from '@react-native-firebase/database';

// database()
//   .setPersistenceEnabled(true)
//   .then(() => console.log('Realtime Database persistence enabled'));

AppRegistry.registerComponent(appName, () => App);
